USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_Adicionar]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [SPPACO_Seg_Utilizador_Adicionar] newid(), 1, 'testevisit', '$p4s7i10#', 'cpereira@utad.pt', '[TESTES - VISIT] Cláudio Pereira', 3
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_Adicionar]
	@IUPI uniqueidentifier = null,
	@Visit bit,
	@UU nvarchar(100),
	@Password nvarchar(max) = null,
	@Email nvarchar(100) = @UU,
	@Nome nvarchar(300) = NULL,
	@IDGrupo int,
	@DataLimite datetime = null,
	@numeroMec varchar(50) = null
AS
	--DECLARE @IUPI uniqueidentifier = 'ede6a4c4-b271-41fe-a6e7-fdc09fd6a807'
	--DECLARE @IUPI uniqueidentifier = '9f289be5-81aa-4959-95ed-7f0aba18b884'
	--DECLARE @IUPI uniqueidentifier = '5b15a21a-dc0d-4d19-a34f-fef03d65ed80'
	--DECLARE @IUPI uniqueidentifier = '632a1335-282f-4405-95d9-895a2608c16a', @UU nvarchar(100) = 'carinafsilva@ua.pt', @Nome nvarchar(300) = 'Carina Freitas da Silva', @NumeroSIGAcad int = 41728
	DECLARE @IDUtilizador bigint,
			@IUPIStr varchar(50),
			@datalim datetime

	SET @datalim = isnull(@DataLimite, CONVERT(datetime, '31-12-9999', 105))
	SET @IUPI = isnull(@IUPI, newid())

	SET @IUPIStr = CONVERT(varchar(50), @IUPI)
	
	SELECT TOP 1 @IDUtilizador = U.IDUtilizador 
	FROM TBL_PACO_SEG_UTILIZADOR U
	WHERE U.Password = @IUPIStr
	
	IF @IDUtilizador IS NULL
		SELECT TOP 1 @IDUtilizador = IDUtilizador
		FROM TblSegUtilizadorIUPI I
		WHERE I.IUPI = @IUPI
	
	IF @IDUtilizador IS NOT NULL
	BEGIN
		RAISERROR('Já existe um utilizador com o IUPI pedido. IUPI=%s IDUtilizador=%I64d', 16, 1, @IUPIStr, @IDUtilizador)
		RETURN
	END
	
	IF @Nome IS NULL
		SET @Nome = ''

	BEGIN TRY
		BEGIN TRAN
		
		if @Visit = 1
		begin
			declare 
				@PwdHash binary(64),
				@Salt uniqueidentifier = newid()

			set @PwdHash = HASHBYTES('SHA1', @Password + cast(@Salt as nvarchar(36)))
			/*
			O salt é guardado porque o que interessa é ter passwords únicas, que não apareça em nenhuma rainbow table. Ao usar uniqueidentifier para o salt, em junção com a password, é praticamente garantida a segurança das hashes
			*/
			INSERT INTO TBL_PACO_SEG_UTILIZADOR (Login, Password, Salt, TipoUtilizador, NumTentativa, EstadoActual, Email)
				VALUES (@UU, @PwdHash, @Salt, 5, 0, 1, @Email)
		end 
		else
		begin
			INSERT INTO TBL_PACO_SEG_UTILIZADOR (Login, TipoUtilizador, NumTentativa, EstadoActual, Email)
			VALUES (@UU, 5, 0, 1, @Email)
		end
		
		SET @IDUtilizador = SCOPE_IDENTITY()
		
		--INSERT INTO TBL_PACO_SEG_UTILIZADOR_REGISTADO (IDUtilizador, NomeUtilizadorRegistado)
		--	VALUES (@IDUtilizador, @Nome)

		INSERT INTO TblSegUtilizador (IDUtilizador, Nome)
			VALUES (@IDUtilizador, @Nome)
		
		INSERT INTO TblSegUtilizadorIUPI (IDUtilizador, IUPI, Visit)
			VALUES (@IDUtilizador, @IUPI, @Visit)
		
		INSERT INTO TblSegAssUtilizadorGrupo (IDUtilizador, IDGrupo, DataInicial, DataFinal)
			VALUES (@IDUtilizador, @IDGrupo, GETDATE(), CONVERT(datetime, @datalim, 105))
		
		INSERT INTO TblSegAssUtilizadorEstado (IDUtilizador, DataInicial, DataFinal, Activo)
			VALUES (@IDUtilizador, GETDATE(), @datalim, 1)
			
		IF NOT EXISTS (SELECT * FROM academ_seguranca.dbo.Tbl_PACO_Utilizador U WHERE U.IDUtilizador = @IDUtilizador)
		BEGIN
			INSERT INTO academ_seguranca.dbo.Tbl_PACO_Utilizador (IDUtilizador, Nome, Email,NumFuncionario)
				VALUES (@IDUtilizador, @Nome, @Email,@numeroMec)
		END
		ELSE
		BEGIN
			UPDATE academ_seguranca.dbo.Tbl_PACO_Utilizador SET
				Nome = @Nome,
				Email = @Email
			WHERE IDUtilizador = @IDUtilizador
		END

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();	
	        
		ROLLBACK TRAN

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );

		SELECT
		ERROR_NUMBER() AS ErrorNumber
		,@UU as UU
		,@IUPI as IUPI
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH


GO
