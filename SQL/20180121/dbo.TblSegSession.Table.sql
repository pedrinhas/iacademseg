USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegSession]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegSession](
	[SessionId] [nvarchar](88) NOT NULL,
	[IDServico] [int] NOT NULL,
	[IDUtilizador] [bigint] NULL,
	[IDUtilizadorHistoricoAcesso] [bigint] NULL,
	[Created] [datetime] NOT NULL,
	[Expires] [datetime] NOT NULL,
	[Timeout] [int] NOT NULL,
	[LastAccess] [datetime] NOT NULL,
	[IDWebserver] [smallint] NULL,
 CONSTRAINT [PK_TblSegUtilizadorSessions] PRIMARY KEY CLUSTERED 
(
	[SessionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSegSession]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizadorSessions_IDServico] FOREIGN KEY([IDServico])
REFERENCES [dbo].[TblSegServico] ([IDServico])
GO
ALTER TABLE [dbo].[TblSegSession] CHECK CONSTRAINT [FK_TblSegUtilizadorSessions_IDServico]
GO
ALTER TABLE [dbo].[TblSegSession]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizadorSessions_IDUtilizador] FOREIGN KEY([IDUtilizador])
REFERENCES [dbo].[TblSegUtilizador] ([IDUtilizador])
GO
ALTER TABLE [dbo].[TblSegSession] CHECK CONSTRAINT [FK_TblSegUtilizadorSessions_IDUtilizador]
GO
ALTER TABLE [dbo].[TblSegSession]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizadorSessions_IDUtilizadorHistoricoAcesso] FOREIGN KEY([IDUtilizadorHistoricoAcesso])
REFERENCES [dbo].[TblSegUtilizadorHistoricoAcesso] ([IDUtilizadorHistoricoAcesso])
GO
ALTER TABLE [dbo].[TblSegSession] CHECK CONSTRAINT [FK_TblSegUtilizadorSessions_IDUtilizadorHistoricoAcesso]
GO
ALTER TABLE [dbo].[TblSegSession]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizadorSessions_IDWebserver] FOREIGN KEY([IDWebserver])
REFERENCES [dbo].[TblSegWebserver] ([IDWebserver])
GO
ALTER TABLE [dbo].[TblSegSession] CHECK CONSTRAINT [FK_TblSegUtilizadorSessions_IDWebserver]
GO
