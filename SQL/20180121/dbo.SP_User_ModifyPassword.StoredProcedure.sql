USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SP_User_ModifyPassword]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_User_ModifyPassword]
	@login nvarchar(max)
	, @newPassword nvarchar(max)
as
	
	declare @newSalt uniqueidentifier,
		@pwdBytes binary(64)

	set @newSalt = newid()

	set @pwdBytes = HASHBYTES('SHA1', @newPassword + cast(@newSalt as nvarchar(36)))

	update TBL_PACO_SEG_UTILIZADOR
	set Password = @pwdBytes, Salt = @newSalt
	where login = @login


GO
