USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_InfoUtilizador_Obter]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 04-10-2011
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_InfoUtilizador_Obter]
	@IDUtilizador bigint
AS
	SELECT IU.IDUtilizador, IU.Nome, IU.Email, IU.NumAluno, IU.NumFuncionario FROM academ_seguranca.dbo.Tbl_PACO_Utilizador IU
	INNER JOIN TblSegUtilizador U ON U.IDUtilizador = IU.IDUtilizador
	WHERE U.IDUtilizador = @IDUtilizador


GO
