USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[createAlunoIUPI]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[createAlunoIUPI]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- exec createAlunoIUPI

    -- Insert statements for procedure here
declare
@numero varchar(10)	
	
DECLARE Aluno_Cursor CURSOR FOR
SELECT numero 
FROM inscricoes2sigacad.dbo.tblaluno WHERE NUMERO<>27067;
OPEN Aluno_Cursor;
FETCH NEXT FROM Aluno_Cursor into @numero;
WHILE @@FETCH_STATUS = 0
   BEGIN
   EXEC dbo.stp_Get_Aluno4Account_S @numero  
      FETCH NEXT FROM Aluno_Cursor into @numero;
   END;
CLOSE Aluno_Cursor;
DEALLOCATE Aluno_Cursor;

	
	
	
END

GO
