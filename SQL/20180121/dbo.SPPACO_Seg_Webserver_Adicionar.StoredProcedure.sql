USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Webserver_Adicionar]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 2015-02-28
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Webserver_Adicionar]
	@Hostname nvarchar(50)
AS
	SET NOCOUNT ON


	DECLARE @TranCounter INT;
    SET @TranCounter = @@TRANCOUNT;
    IF @TranCounter > 0
		SAVE TRANSACTION main
	ELSE 
		BEGIN TRAN
	
	BEGIN TRY

		DECLARE @IDWebserver smallint

		SELECT TOP 1 @IDWebserver = W.IDWebserver FROM TblSegWebserver W WHERE W.Hostname = @Hostname

		IF @IDWebserver IS NULL
		BEGIN

			DECLARE @Now datetime = GETDATE()
			
			INSERT INTO TblSegWebserver (Hostname, Created)
				VALUES (@Hostname, @Now)

			SET @IDWebserver = SCOPE_IDENTITY()

		END

		SELECT @IDWebserver AS IDWebserver

		IF @TranCounter = 0
			COMMIT TRAN
	END TRY
	BEGIN CATCH
		-- An error occurred; must determine
        -- which type of rollback will roll
        -- back only the work done in the
        -- procedure.
        IF @TranCounter = 0
            -- Transaction started in procedure.
            -- Roll back complete transaction.
            ROLLBACK TRANSACTION;
        ELSE
            -- Transaction started before procedure
            -- called, do not roll back modifications
            -- made before the procedure was called.
            IF XACT_STATE() <> -1
                -- If the transaction is still valid, just
                -- roll back to the savepoint set at the
                -- start of the stored procedure.
                ROLLBACK TRANSACTION main;
                -- If the transaction is uncommitable, a
                -- rollback to the savepoint is not allowed
                -- because the savepoint rollback writes to
                -- the log. Just return to the caller, which
                -- should roll back the outer transaction.
                
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		---- Use RAISERROR inside the CATCH block to return error
		---- information about the original error that caused
		---- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH


GO
