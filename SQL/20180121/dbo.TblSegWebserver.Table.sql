USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegWebserver]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegWebserver](
	[IDWebserver] [smallint] IDENTITY(1,1) NOT NULL,
	[Hostname] [nvarchar](50) NOT NULL,
	[Created] [datetime] NOT NULL,
 CONSTRAINT [PK_TblSegWebserver] PRIMARY KEY CLUSTERED 
(
	[IDWebserver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
