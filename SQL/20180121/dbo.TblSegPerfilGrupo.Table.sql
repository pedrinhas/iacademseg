USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegPerfilGrupo]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegPerfilGrupo](
	[IDPerfilGrupo] [smallint] IDENTITY(1,1) NOT NULL,
	[NomePerfilGrupo] [nvarchar](150) NOT NULL,
	[Ordem] [smallint] NULL,
 CONSTRAINT [PK_TblSegPerfilGrupo] PRIMARY KEY CLUSTERED 
(
	[IDPerfilGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
