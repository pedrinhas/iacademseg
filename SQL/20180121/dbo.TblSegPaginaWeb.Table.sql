USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegPaginaWeb]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegPaginaWeb](
	[IDPaginaWeb] [int] IDENTITY(1,1) NOT NULL,
	[IDServico] [int] NOT NULL,
	[TituloPaginaWeb] [nvarchar](150) NOT NULL,
	[DescricaoPaginaWeb] [nvarchar](255) NOT NULL,
	[NomeFicheiro] [nvarchar](100) NOT NULL,
	[URL] [nvarchar](255) NOT NULL,
	[ComRestricao] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
	[IDBanner] [int] NOT NULL,
 CONSTRAINT [PK_TblSegPaginaWeb] PRIMARY KEY CLUSTERED 
(
	[IDPaginaWeb] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSegPaginaWeb]  WITH CHECK ADD  CONSTRAINT [FK_TblSegPaginaWeb_IDServico] FOREIGN KEY([IDServico])
REFERENCES [dbo].[TblSegServico] ([IDServico])
GO
ALTER TABLE [dbo].[TblSegPaginaWeb] CHECK CONSTRAINT [FK_TblSegPaginaWeb_IDServico]
GO
