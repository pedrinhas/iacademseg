USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_AdicionarIUPI]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 05-06-2011
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_AdicionarIUPI]
	@NumeroSIGAcad varchar(50),
	@IDUtilizador bigint = NULL,
	@IUPI uniqueidentifier,
	@Visit bit,
	@UU nvarchar(100) = NULL,
	@Nome nvarchar(300) = NULL
AS
	--DECLARE @IUPI uniqueidentifier = 'ede6a4c4-b271-41fe-a6e7-fdc09fd6a807'
	--DECLARE @IUPI uniqueidentifier = '9f289be5-81aa-4959-95ed-7f0aba18b884'
	--DECLARE @IUPI uniqueidentifier = '5b15a21a-dc0d-4d19-a34f-fef03d65ed80'
	--DECLARE @NumeroSIGAcad int = 55910, @IDUtilizador bigint = 64340, @IUPI uniqueidentifier = 'FD27A438-B985-4D13-8BE9-F33ECD2B28EF', @Visit bit = false, @UU nvarchar(100) = null, @Nome nvarchar(300) = null
	
	DECLARE @IDUtilizadorN bigint,
			@IUPIStr varchar(50),
			@AdicionarIUPI bit = 1
	
	BEGIN TRY
		BEGIN TRAN
	
		SET @IUPIStr = CONVERT(varchar(50), @IUPI)
		
		SELECT TOP 1 @IDUtilizadorN = U.IDUtilizador 
		FROM TBL_PACO_SEG_UTILIZADOR U
		WHERE U.Password = @IUPIStr
		
		IF @IDUtilizadorN IS NULL
			SELECT TOP 1 @IDUtilizadorN = IDUtilizador
			FROM TblSegUtilizadorIUPI I
			WHERE I.IUPI = @IUPI
		
		IF @IDUtilizadorN IS NOT NULL
		BEGIN
			IF @IDUtilizadorN = @IDUtilizador
			BEGIN
				SET @AdicionarIUPI = 0
			END
			ELSE
			BEGIN
				RAISERROR('Já existe um utilizador com o IUPI pedido. IUPI=%s IDUtilizador=%I64d IDUtilizadorN=%I64d', 16, 1, @IUPIStr, @IDUtilizador, @IDUtilizadorN)
				RETURN
			END
		END
		
		IF @IDUtilizador IS NULL
		BEGIN
			SELECT TOP 1 @IDUtilizador = C.IDUtilizador	
			FROM academ_seguranca.dbo.Tbl_PACO_MatOn_Colocacao C
			WHERE C.NumeroSIGAcad = @NumeroSIGAcad
			
			IF @IDUtilizador IS NULL
			BEGIN
				RAISERROR('Não foi possível obter o IDUtilizador do utilizador. NumeroSIGAcad=%d', 16, 1, @NumeroSIGAcad)
				RETURN
			END
		END

		IF @UU IS NOT NULL
		BEGIN
			IF EXISTS (SELECT * FROM TBL_PACO_SEG_UTILIZADOR U 
				WHERE U.IDUtilizador != @IDUtilizador AND U.Login LIKE @UU)
			BEGIN
				RAISERROR('Já existe um utilizador com o UU pedido. UU=%s', 16, 1, @UU)
				RETURN
			END
		END

		
		IF @Nome IS NOT NULL
		BEGIN
			UPDATE TblSegUtilizador SET Nome = @Nome
			WHERE IDUtilizador = @IDUtilizador
			
			UPDATE TBL_PACO_SEG_UTILIZADOR_REGISTADO SET NomeUtilizadorRegistado = @Nome
			WHERE IDUtilizador = @IDUtilizador
		END
		
		IF @UU IS NOT NULL
		BEGIN
			UPDATE TBL_PACO_SEG_UTILIZADOR SET Login = @UU, Password = @IUPI
			WHERE IDUtilizador = @IDUtilizador
		END
		ELSE
		BEGIN
			UPDATE TBL_PACO_SEG_UTILIZADOR SET Password = @IUPI
				WHERE IDUtilizador = @IDUtilizador
		END
			
		IF NOT EXISTS (SELECT * FROM academ_seguranca.dbo.Tbl_PACO_Utilizador U WHERE U.IDUtilizador = @IDUtilizador)
		BEGIN
			INSERT INTO academ_seguranca.dbo.Tbl_PACO_Utilizador (IDUtilizador, Nome)
				VALUES (@IDUtilizador, @Nome)
		END
		ELSE
		BEGIN
			IF @Nome IS NOT NULL
			BEGIN
				UPDATE academ_seguranca.dbo.Tbl_PACO_Utilizador SET
					Nome = @Nome
				WHERE IDUtilizador = @IDUtilizador
			END
		END
		
		IF NOT EXISTS (SELECT * FROM TblSegUtilizadorIUPI I WHERE I.IDUtilizador = @IDUtilizador AND I.IUPI = @IUPI)
		BEGIN
			INSERT INTO TblSegUtilizadorIUPI (IDUtilizador, IUPI, Visit)
				VALUES (@IDUtilizador, @IUPI, @Visit)
		END

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();	
	        
		ROLLBACK TRAN

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH


GO
