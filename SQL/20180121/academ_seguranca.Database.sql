USE [master]
GO
/****** Object:  Database [academ_seguranca]    Script Date: 23/01/2018 10:42:32 AM ******/
CREATE DATABASE [academ_seguranca] ON  PRIMARY 
( NAME = N'academ_seguranca', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\academ_seguranca.mdf' , SIZE = 60672KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'academ_seguranca_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\academ_seguranca_log.LDF' , SIZE = 770752KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [academ_seguranca] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [academ_seguranca].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [academ_seguranca] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [academ_seguranca] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [academ_seguranca] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [academ_seguranca] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [academ_seguranca] SET ARITHABORT OFF 
GO
ALTER DATABASE [academ_seguranca] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [academ_seguranca] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [academ_seguranca] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [academ_seguranca] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [academ_seguranca] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [academ_seguranca] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [academ_seguranca] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [academ_seguranca] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [academ_seguranca] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [academ_seguranca] SET  ENABLE_BROKER 
GO
ALTER DATABASE [academ_seguranca] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [academ_seguranca] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [academ_seguranca] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [academ_seguranca] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [academ_seguranca] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [academ_seguranca] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [academ_seguranca] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [academ_seguranca] SET RECOVERY FULL 
GO
ALTER DATABASE [academ_seguranca] SET  MULTI_USER 
GO
ALTER DATABASE [academ_seguranca] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [academ_seguranca] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'academ_seguranca', N'ON'
GO
ALTER DATABASE [academ_seguranca] SET  READ_WRITE 
GO
