USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSysMenuIcon]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblSysMenuIcon](
	[MenuID] [int] NOT NULL,
	[DescIcon] [varchar](100) NOT NULL,
	[ImgOver] [varchar](200) NOT NULL,
	[ImgOut] [varchar](200) NOT NULL,
	[Localizacao] [smallint] NOT NULL,
 CONSTRAINT [PK_TblSysMenuIcon] PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TblSysMenuIcon]  WITH CHECK ADD  CONSTRAINT [FK_TblSysMenuIcon_TblSysMenu] FOREIGN KEY([MenuID])
REFERENCES [dbo].[TblSysMenu] ([MenuId])
GO
ALTER TABLE [dbo].[TblSysMenuIcon] CHECK CONSTRAINT [FK_TblSysMenuIcon_TblSysMenu]
GO
