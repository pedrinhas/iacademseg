USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SP_User_Exists]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cláudio Pereira
-- Create date: 05-05-2016
-- Description:	Verificar se um utilizador já existe
-- =============================================
CREATE PROCEDURE [dbo].[SP_User_Exists]
	@UU nvarchar(max)
AS
BEGIN
	SELECT [IDUtilizador] from [dbo].[TBL_PACO_SEG_UTILIZADOR] where [Login] = @UU
END

GO
