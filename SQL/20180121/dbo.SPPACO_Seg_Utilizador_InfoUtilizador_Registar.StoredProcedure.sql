USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_InfoUtilizador_Registar]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 04-10-2011
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_InfoUtilizador_Registar]
	@IDUtilizador bigint,
	@Nome nvarchar(300) = NULL,
	@Email nvarchar(256) = NULL,
	@NumAluno int = NULL,
	@NumFuncionario int = NULL
AS
	BEGIN TRY
		BEGIN TRAN
		
		DECLARE @CNome nvarchar(300) = NULL,
				@CEmail nvarchar(256) = NULL,
				@CNumAluno int = NULL,
				@CNumFuncionario int = NULL
		
		IF NOT EXISTS (SELECT * FROM academ_seguranca.dbo.Tbl_PACO_Utilizador WHERE IDUtilizador = @IDUtilizador)
		BEGIN
			INSERT INTO academ_seguranca.dbo.Tbl_PACO_Utilizador (IDUtilizador, Nome, Email, NumAluno, NumFuncionario)
				VALUES (@IDUtilizador, @Nome, @Email, @NumAluno, @NumFuncionario)
		END
		ELSE
		BEGIN
			SELECT TOP 1 @CNome = PU.Nome, @CEmail = PU.Email, @CNumAluno = PU.NumAluno, @CNumFuncionario = PU.NumFuncionario 
				FROM academ_seguranca.dbo.Tbl_PACO_Utilizador PU WHERE IDUtilizador = @IDUtilizador
		
			IF @Nome IS NOT NULL
				SET @CNome = @Nome

			IF @Email IS NOT NULL
				SET @CEmail = @Email

			IF @NumAluno IS NOT NULL
				SET @CNumAluno = @NumAluno

			IF @NumFuncionario IS NOT NULL
				SET @CNumFuncionario = @NumFuncionario
				
			
			UPDATE academ_seguranca.dbo.Tbl_PACO_Utilizador SET
					Nome = @CNome,
					Email = @CEmail,
					NumAluno = @CNumAluno,
					NumFuncionario = @CNumFuncionario
			WHERE IDUtilizador = @IDUtilizador
		END
		
		IF @Nome IS NOT NULL
		BEGIN
			UPDATE academ_seguranca.dbo.TblSegUtilizador SET
					Nome = @Nome
			WHERE IDUtilizador = @IDUtilizador
			
			UPDATE academ_seguranca.dbo.TBL_PACO_SEG_UTILIZADOR_REGISTADO SET
					NomeUtilizadorRegistado = @Nome
			WHERE IDUtilizador = @IDUtilizador
		END

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();	
	        
		ROLLBACK TRAN

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH


GO
