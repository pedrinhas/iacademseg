USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSysResourceString]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSysResourceString](
	[IDResourceString] [int] IDENTITY(1,1) NOT NULL,
	[IDResourceKey] [int] NOT NULL,
	[IDCulture] [smallint] NOT NULL,
	[ResourceValue] [nvarchar](4000) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[ChangeDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TblSysResourceString] PRIMARY KEY CLUSTERED 
(
	[IDResourceString] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSysResourceString]  WITH CHECK ADD  CONSTRAINT [FK_TblSysResourceString_IDCulture] FOREIGN KEY([IDCulture])
REFERENCES [dbo].[TblSysCulture] ([IDCulture])
GO
ALTER TABLE [dbo].[TblSysResourceString] CHECK CONSTRAINT [FK_TblSysResourceString_IDCulture]
GO
ALTER TABLE [dbo].[TblSysResourceString]  WITH CHECK ADD  CONSTRAINT [FK_TblSysResourceString_IDResourceKey] FOREIGN KEY([IDResourceKey])
REFERENCES [dbo].[TblSysResourceKey] ([IDResourceKey])
GO
ALTER TABLE [dbo].[TblSysResourceString] CHECK CONSTRAINT [FK_TblSysResourceString_IDResourceKey]
GO
