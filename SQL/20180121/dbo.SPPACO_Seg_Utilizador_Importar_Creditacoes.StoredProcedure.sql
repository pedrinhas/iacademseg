USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_Importar_Creditacoes]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 12-09-2011
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_Importar_Creditacoes]
	@IUPI uniqueidentifier,
	@Visit bit,
	@UU nvarchar(100) = NULL,
	@Nome nvarchar(300) = NULL
AS
	--DECLARE @IUPI uniqueidentifier = 'ede6a4c4-b271-41fe-a6e7-fdc09fd6a807'
	--DECLARE @IUPI uniqueidentifier = '9f289be5-81aa-4959-95ed-7f0aba18b884'
	--DECLARE @IUPI uniqueidentifier = '5b15a21a-dc0d-4d19-a34f-fef03d65ed80'
	--DECLARE @IUPI uniqueidentifier = 'c27ee415-94ed-402e-b487-899f50b9fa70', @UU nvarchar(100), @Nome nvarchar(300)
	DECLARE @IDUtilizador bigint,
			@IUPIStr varchar(50),
			@NumMecAluno int
			
	
	SET @IUPIStr = CONVERT(varchar(50), @IUPI)
			
	SELECT TOP 1 @IDUtilizador = IDUtilizador
	FROM TBL_PACO_SEG_UTILIZADOR U
	WHERE U.Password = @IUPIStr
	
	IF @IDUtilizador IS NULL
		SELECT TOP 1 @IDUtilizador = IDUtilizador
		FROM TBL_PACO_SEG_UTILIZADOR U
		WHERE U.Login LIKE @UU
			
	SELECT TOP 1 @NumMecAluno = Numero
	FROM academ_seguranca.dbo.TblIUPIUtilizador I 
	WHERE I.IUPI = @IUPI AND I.IDTipoUtilizador = 1
	
	IF @Nome IS NULL
		SELECT TOP 1 @Nome = Nome
		FROM academ_seguranca.dbo.TblAluno A 
		WHERE A.NUMERO = @NumMecAluno
	
	IF @IDUtilizador IS NULL AND @NumMecAluno IS NULL
	BEGIN
		RAISERROR('Não foi possível obter o IDUtilizador ou NMecAluno do utilizador. IUPI=%s', 16, 1, @IUPIStr)
		RETURN
	END
	
	IF @Nome IS NULL AND @IDUtilizador IS NOT NULL
		SELECT TOP 1 @Nome = UR.NomeUtilizadorRegistado
		FROM TBL_PACO_SEG_UTILIZADOR_REGISTADO UR
		WHERE UR.IDUtilizador = @IDUtilizador
	
	IF @Nome IS NULL
		SET @Nome = ''
		
	print @IDUtilizador
	
	BEGIN TRY
		BEGIN TRAN
		
		IF @IDUtilizador IS NULL
		BEGIN
			INSERT INTO TBL_PACO_SEG_UTILIZADOR (Login, Password, TipoUtilizador, NumTentativa, EstadoActual, Email)
				VALUES (@UU, @IUPIStr, 5, 0, 1, @UU)
			
			SET @IDUtilizador = SCOPE_IDENTITY()
			
			INSERT INTO TBL_PACO_SEG_UTILIZADOR_REGISTADO (IDUtilizador, NomeUtilizadorRegistado)
				VALUES (@IDUtilizador, @Nome)
		END
			

		IF NOT EXISTS (SELECT * FROM TblSegUtilizador U WHERE U.IDUtilizador = @IDUtilizador)
		BEGIN
			INSERT INTO TblSegUtilizador (IDUtilizador, Nome)
				VALUES (@IDUtilizador, @Nome)
			
			INSERT INTO TblSegUtilizadorIUPI (IDUtilizador, IUPI, Visit)
				VALUES (@IDUtilizador, @IUPI, @Visit)
			
			INSERT INTO TblSegAssUtilizadorGrupo (IDUtilizador, IDGrupo, DataInicial, DataFinal)
				VALUES (@IDUtilizador, 3, GETDATE(), CONVERT(datetime, '31-12-9999', 105))
			
			INSERT INTO TblSegAssUtilizadorEstado (IDUtilizador, DataInicial, DataFinal, Activo)
				VALUES (@IDUtilizador, GETDATE(), CONVERT(datetime, '31-12-9999', 105), 1)
		END
		--ELSE
		--BEGIN
		--	IF NOT EXISTS (SELECT * FROM TblSegUtilizadorIUPI I WHERE I.IUPI = @IUPI)
		--	BEGIN
		--		INSERT INTO TblSegUtilizadorIUPI (IDUtilizador, IUPI)
		--			VALUES (@IDUtilizador, @IUPI)
		--	END
			
		--	IF NOT EXISTS (SELECT * FROM TblSegAssUtilizadorGrupo UG WHERE UG.IDUtilizador = @IDUtilizador AND UG.IDGrupo = 3 
		--		AND UG.DataFinal <= GETDATE() AND UG.DataInicial >= GETDATE())
		--	BEGIN
		--		INSERT INTO TblSegAssUtilizadorGrupo (IDUtilizador, IDGrupo, DataInicial, DataFinal)
		--			VALUES (@IDUtilizador, 3, GETDATE(), CONVERT(datetime, '31-12-9999', 105))
		--	END
			
		--	IF NOT EXISTS (SELECT * FROM TblSegAssUtilizadorEstado UE WHERE UE.IDUtilizador = @IDUtilizador AND UE.Activo = 1
		--		AND UE.DataFinal <= GETDATE() AND UE.DataInicial >= GETDATE())
		--	BEGIN
		--		INSERT INTO TblSegAssUtilizadorEstado (IDUtilizador, DataInicial, DataFinal, Activo)
		--			VALUES (@IDUtilizador, GETDATE(), CONVERT(datetime, '31-12-9999', 105), 1)
		--	END
		--END
			
		IF NOT EXISTS (SELECT * FROM academ_seguranca.dbo.Tbl_PACO_Utilizador U WHERE U.IDUtilizador = @IDUtilizador)
		BEGIN
			INSERT INTO academ_seguranca.dbo.Tbl_PACO_Utilizador (IDUtilizador, Nome, NumAluno)
				VALUES (@IDUtilizador, @Nome, @NumMecAluno)
		END
		ELSE
		BEGIN
			UPDATE academ_seguranca.dbo.Tbl_PACO_Utilizador SET
				Nome = @Nome,
				NumAluno = @NumMecAluno
			WHERE IDUtilizador = @IDUtilizador
		END

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();	
	        
		ROLLBACK TRAN

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH


GO
