USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_Aluno_Obter]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 12-09-2011
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_Aluno_Obter]
	@IUPI uniqueidentifier
AS
	SELECT TOP 1 A.Nome, A.Numero FROM academ_seguranca.dbo.TblIUPIUtilizador I 
	INNER JOIN inscricoes2SIGACAD.dbo.TblAluno A ON A.Numero = I.Numero
	WHERE I.IUPI = @IUPI AND I.IDTipoUtilizador = 1


GO
