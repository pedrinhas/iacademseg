USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SP_LocalLogin_Impersonate]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--exec [dbo].[SP_LocalLogin_DoLogin] 'al0001', 'pass'

CREATE proc [dbo].[SP_LocalLogin_Impersonate]
@Username nvarchar(250)
as
begin try
	declare @now datetime = getdate()

	SELECT SEGU.IDUtilizador, U.Nome, SEGU.Login, SEGU.Email, I.IUPI
		FROM [dbo].[TBL_PACO_SEG_UTILIZADOR] SEGU
		left join [dbo].[TblSegUtilizador] U on SEGU.[IDUtilizador] = U.[IDUtilizador]
		left join [dbo].[TblSegAssUtilizadorEstado] E on SEGU.IDUtilizador = E.IDUtilizador
		left join [dbo].[TblSegUtilizadorIUPI] I on SEGU.IDUtilizador = I.IDUtilizador
		where
			SEGU.Login = @Username COLLATE SQL_Latin1_General_CP1_CS_AS
			and E.Activo = 1
			and @now > E.DataInicial
			and @now < E.DataFinal
end try
begin catch

    select ERROR_NUMBER() AS ErrorNumber
    ,@Username as Username
    ,ERROR_SEVERITY() AS ErrorSeverity
    ,ERROR_STATE() AS ErrorState
    ,ERROR_PROCEDURE() AS ErrorProcedure
    ,ERROR_LINE() AS ErrorLine
    ,ERROR_MESSAGE() AS ErrorMessage;

end catch
GO
