USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblBannersCorIndividual]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblBannersCorIndividual](
	[IDBanner] [int] NOT NULL,
	[IDCorLeft] [smallint] NOT NULL,
	[IDCorRight] [smallint] NOT NULL,
	[IDCorRightMiddle] [smallint] NOT NULL,
	[IDLogo] [smallint] NOT NULL,
 CONSTRAINT [PK_TblBannersCorIndividual] PRIMARY KEY CLUSTERED 
(
	[IDBanner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblBannersCorIndividual]  WITH CHECK ADD  CONSTRAINT [FK_TblBannersCorIndividual_TblBanners] FOREIGN KEY([IDBanner])
REFERENCES [dbo].[TblBanners] ([IDBanner])
GO
ALTER TABLE [dbo].[TblBannersCorIndividual] CHECK CONSTRAINT [FK_TblBannersCorIndividual_TblBanners]
GO
ALTER TABLE [dbo].[TblBannersCorIndividual]  WITH CHECK ADD  CONSTRAINT [FK_TblBannersCorIndividual_TblSysCorBanners] FOREIGN KEY([IDCorLeft])
REFERENCES [dbo].[TblSysCorBanners] ([IDCorBanner])
GO
ALTER TABLE [dbo].[TblBannersCorIndividual] CHECK CONSTRAINT [FK_TblBannersCorIndividual_TblSysCorBanners]
GO
ALTER TABLE [dbo].[TblBannersCorIndividual]  WITH CHECK ADD  CONSTRAINT [FK_TblBannersCorIndividual_TblSysCorBanners1] FOREIGN KEY([IDCorRight])
REFERENCES [dbo].[TblSysCorBanners] ([IDCorBanner])
GO
ALTER TABLE [dbo].[TblBannersCorIndividual] CHECK CONSTRAINT [FK_TblBannersCorIndividual_TblSysCorBanners1]
GO
ALTER TABLE [dbo].[TblBannersCorIndividual]  WITH CHECK ADD  CONSTRAINT [FK_TblBannersCorIndividual_TblSysCorBanners2] FOREIGN KEY([IDCorRightMiddle])
REFERENCES [dbo].[TblSysCorBanners] ([IDCorBanner])
GO
ALTER TABLE [dbo].[TblBannersCorIndividual] CHECK CONSTRAINT [FK_TblBannersCorIndividual_TblSysCorBanners2]
GO
