USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegUtilizadorHistoricoAcesso]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblSegUtilizadorHistoricoAcesso](
	[IDUtilizadorHistoricoAcesso] [bigint] IDENTITY(1,1) NOT NULL,
	[IDUtilizador] [bigint] NOT NULL,
	[EnderecoIP] [char](50) NOT NULL,
	[Data] [datetime] NOT NULL,
	[UserAgent] [nvarchar](250) NOT NULL,
	[IDServico] [int] NOT NULL,
	[UU] [nvarchar](100) NULL,
 CONSTRAINT [PK_TblSegUtilizadorHistoricoAcesso] PRIMARY KEY CLUSTERED 
(
	[IDUtilizadorHistoricoAcesso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TblSegUtilizadorHistoricoAcesso]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizadorHistoricoAcesso_IDServico] FOREIGN KEY([IDServico])
REFERENCES [dbo].[TblSegServico] ([IDServico])
GO
ALTER TABLE [dbo].[TblSegUtilizadorHistoricoAcesso] CHECK CONSTRAINT [FK_TblSegUtilizadorHistoricoAcesso_IDServico]
GO
ALTER TABLE [dbo].[TblSegUtilizadorHistoricoAcesso]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizadorHistoricoAcesso_TblSegUtilizador] FOREIGN KEY([IDUtilizador])
REFERENCES [dbo].[TblSegUtilizador] ([IDUtilizador])
GO
ALTER TABLE [dbo].[TblSegUtilizadorHistoricoAcesso] CHECK CONSTRAINT [FK_TblSegUtilizadorHistoricoAcesso_TblSegUtilizador]
GO
