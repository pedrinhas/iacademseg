USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Session_Obter]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 2015-02-28
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Session_Obter]
	@SessionId nvarchar(88),
	@IDServico int
AS
	SET NOCOUNT ON

	SELECT S.SessionId, S.IDServico, S.IDUtilizador, S.IDUtilizadorHistoricoAcesso, S.Created, S.Expires, S.Timeout, S.LastAccess,
		U.Nome, H.EnderecoIP, H.UserAgent, H.UU,
		W.IDWebserver, W.Hostname
		--,CASE WHEN TS.SessionItemShort IS NULL THEN TS.SessionItemLong ELSE TS.SessionItemShort END AS SessionItem
	FROM TblSegSession S
	LEFT JOIN TblSegUtilizador U ON U.IDUtilizador = S.IDUtilizador
	LEFT JOIN TblSegUtilizadorHistoricoAcesso H ON H.IDUtilizadorHistoricoAcesso = S.IDUtilizadorHistoricoAcesso
	LEFT JOIN TblSegWebserver W ON W.IDWebserver = S.IDWebserver
	--LEFT JOIN [Sessionstate_inst_v4].dbo.ASPStateTempSessions TS ON TS.SessionId LIKE S.SessionId + '%' COLLATE DATABASE_DEFAULT
	WHERE S.SessionId = @SessionId AND S.IDServico = @IDServico



GO
