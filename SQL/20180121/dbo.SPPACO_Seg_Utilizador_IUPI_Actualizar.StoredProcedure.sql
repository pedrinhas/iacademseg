USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_IUPI_Actualizar]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 02-03-2012
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_IUPI_Actualizar]
	@IDUtilizador bigint,
	@IUPI uniqueidentifier,
	@Visit bit
AS
	UPDATE TblSegUtilizadorIUPI SET Visit = @Visit
		WHERE IDUtilizador = @IDUtilizador AND IUPI = @IUPI


GO
