USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SP_User_Delete_DEV]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_User_Delete_DEV]
@UU nvarchar(max)
as
begin
	begin tran
		begin try
			declare @id int, @allow nvarchar(max)

			select @allow = Value from TblSettings where [Key] = 'AllowDeleteUsers'

			if @allow = '1'
			begin
				select @id = IDUtilizador
				from TBL_PACO_SEG_UTILIZADOR
				where Login = @UU

				delete from Tbl_PACO_Utilizador where IDUtilizador = @id
				delete from TblSegAssUtilizadorEstado where IDUtilizador = @id
				delete from TblSegAssUtilizadorGrupo where IDUtilizador = @id
				delete from TblSegUtilizadorIUPI where IDUtilizador = @id
				delete from TblSegUtilizador where IDUtilizador = @id
				delete from TBL_PACO_SEG_UTILIZADOR where IDUtilizador = @id
			end
			else
			begin
				select  'Error' as [Type], 'TblSettings - AllowDeleteUsers = 0' as [Message]
			end
			commit
		end try
		begin catch
			rollback
		end catch
end
GO
