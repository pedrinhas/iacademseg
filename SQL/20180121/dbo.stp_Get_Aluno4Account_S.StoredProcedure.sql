USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Aluno4Account_S]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<DescripAtion,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Aluno4Account_S]
	@numero varchar(50)

AS
BEGIN

declare
@query varchar(3500)
,@numeroIn varchar(50)
,@newid uniqueidentifier
,@seqID bigint 
,@visit bit
,@UU varchar(100)
,@Nome varchar(300)
,@Email varchar(500)

set @numeroIn=@numero

declare  @temptable table (numero varchar(10),nome varchar(500), e_mail varchar(50), estadomatricula int, telefone varchar(255))

DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

-- exec stp_Get_Aluno4Account_S '22578'
	
set @query='SELECT DISTINCT a.numero,a.nome,
b.e_mail,
1 AS estadoMatricula,
t.telefone FROM tblaluno a
left JOIN tblalunoemail b ON a.numero=b.numero
left join (SELECT numero,  Max(telefone) AS telefone,1 AS defeito FROM tbltelefone  WHERE defeito=1     GROUP BY numero
UNION
SELECT  numero, Max(telefone) AS telefone,0 AS defeito FROM tbltelefone  WHERE numero NOT IN (SELECT  numero FROM tbltelefone  WHERE defeito=1)     GROUP BY numero
) t ON t.numero=a.numero
left JOIN tblmatricula m ON m.numero=a.numero 
WHERE a.numero=''' + @numeroIn + ''''



begin tran
begin try 
	insert into @tempTable
	exec(@query) at SIGACAD

	if exists(select * from @tempTable)
		begin
		if not exists(select numero from [academ_seguranca].[dbo].TblIUPIUtilizador where numero=@numero and idtipoutilizador=3)
			begin
			set @newid=newid()
			set @visit = 0
			--set @UU = 'al' + @numeroIn + '@utad.eu'
			select @Email =  isnull(e_mail,'al' + @numeroIn + '@utad.eu') from @tempTable
			select @UU = 'al' + @numeroIn from @tempTable
			select @Nome=nome from @tempTable
		


			exec [academ_seguranca].[dbo].SPPACO_Seg_Utilizador_Aluno_Adicionar 	@numeroIn ,	@newid, @Visit, 	@UU ,	@Nome , @Email, null


				 set @query='
				 insert into tblrcu_aluno_iupi
				 (idrcu_aluno_iupi,numero,iupi,data)
				 VALUES (seq_idassociaraluno_iupi.nextval,''' + @numeroIn + ''' ,''' + CAST( @newid as varchar(255)) + ''',sysdate)'

				exec(@query) at SIGACAD;
/*
				set @query='SELECT DISTINCT a.numero,a.nome,
				b.e_mail,
				Nvl(m.estadoactual,0) AS estadoMatricula,
				t.telefone FROM tblaluno a
				left JOIN tblalunoemail b ON a.numero=b.numero
				left join (SELECT numero,  Max(telefone) AS telefone,1 AS defeito FROM tbltelefone  WHERE defeito=1     GROUP BY numero
				UNION
				SELECT  numero, Max(telefone) AS telefone,0 AS defeito FROM tbltelefone  WHERE numero NOT IN (SELECT  numero FROM tbltelefone  WHERE defeito=1)     GROUP BY numero
				) t ON t.numero=a.numero
				left JOIN tblmatricula m ON m.numero=a.numero AND (m.estadoactual=1 or m.estadoactual=3)
				WHERE a.numero=''' + @numeroIn + ''''

				exec(@query) at SIGACAD
	*/	
			end
		end
		
	COMMIT tran
end try
begin catch
SELECT
    ERROR_NUMBER() AS ErrorNumber
    ,@numeroIn as numec
    ,ERROR_SEVERITY() AS ErrorSeverity
    ,ERROR_STATE() AS ErrorState
    ,ERROR_PROCEDURE() AS ErrorProcedure
    ,ERROR_LINE() AS ErrorLine
    ,ERROR_MESSAGE() AS ErrorMessage;
	rollback tran	
end catch




END




GO
