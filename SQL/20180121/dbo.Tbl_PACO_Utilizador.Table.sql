USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[Tbl_PACO_Utilizador]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_PACO_Utilizador](
	[IDUtilizador] [bigint] NOT NULL,
	[Nome] [nvarchar](300) NULL,
	[Email] [nvarchar](256) NULL,
	[NumAluno] [int] NULL,
	[NumFuncionario] [int] NULL,
 CONSTRAINT [PK_Tbl_PACO_Utilizador] PRIMARY KEY CLUSTERED 
(
	[IDUtilizador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Tbl_PACO_Utilizador]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_PACO_Utilizador_IDUtilizador] FOREIGN KEY([IDUtilizador])
REFERENCES [dbo].[TblSegUtilizador] ([IDUtilizador])
GO
ALTER TABLE [dbo].[Tbl_PACO_Utilizador] CHECK CONSTRAINT [FK_Tbl_PACO_Utilizador_IDUtilizador]
GO
