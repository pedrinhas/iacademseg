USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Resource_InsertOrUpdate]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joao Taborda
-- Create date: 17-03-2014
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Resource_InsertOrUpdate]
	@IDServico int,
	@ResourceType varchar(100),
	@ResourceKey varchar(100),
	@IDCulture smallint,
	@ResourceValue nvarchar(4000)
AS

	SET NOCOUNT ON
	
	DECLARE @TranCounter INT;
    SET @TranCounter = @@TRANCOUNT;
    IF @TranCounter > 0
		SAVE TRANSACTION main
	ELSE 
		BEGIN TRAN


	BEGIN TRY

		DECLARE @IDResourceKey int, @IDResourceString int
		DECLARE @Date datetime = GETDATE()

		SELECT @IDResourceKey = RK.IDResourceKey
		FROM TblSysResourceKey RK
		INNER JOIN TblSysResource_Servico RS ON RS.IDResourceKey = RK.IDResourceKey
		WHERE RK.ResourceType = @ResourceType AND RS.IDServico = @IDServico AND RK.ResourceKey = @ResourceKey

		IF @IDResourceKey IS NULL
		BEGIN
			INSERT INTO TblSysResourceKey (ResourceType, ResourceKey) 
				VALUES (@ResourceType, @ResourceKey) 

				SET @IDResourceKey = SCOPE_IDENTITY()

			INSERT INTO TblSysResource_Servico (IDResourceKey, IDServico)
				VALUES (@IDResourceKey, @IDServico)
		END

		SELECT @IDResourceString = RS.IDResourceString
		FROM TblSysResourceString RS
		WHERE RS.IDCulture = @IDCulture AND RS.IDResourceKey = @IDResourceKey

		IF @IDResourceString IS NULL
		BEGIN
			INSERT INTO TblSysResourceString (IDCulture, IDResourceKey, ResourceValue, CreationDate, ChangeDate)
				VALUES (@IDCulture, @IDResourceKey, @ResourceValue, @Date, @Date)
		END
		ELSE
		BEGIN
			DECLARE @OldResourceValue nvarchar(4000)

			SELECT @OldResourceValue = RS.ResourceValue
				FROM TblSysResourceString RS 
				WHERE RS.IDResourceString = @IDResourceString
			
			IF @OldResourceValue != @ResourceValue
			BEGIN
				--print 'Resource: Key=' + @ResourceKey + '\nOldValue=' + @OldResourceValue + '\nNewValue=' + @ResourceValue
				IF @IDCulture = 1
				BEGIN
					UPDATE TblSysResourceString SET ResourceValue = @ResourceValue, ChangeDate = @Date
						WHERE IDResourceString = @IDResourceString

					print 'Resource changed: Key=' + @ResourceKey + CHAR(10) + 'OldValue=' + @OldResourceValue +  CHAR(10) + 'NewValue=' + @ResourceValue
				END
				ELSE
				BEGIN
					print 'Resource wasnt updated: Key=' + @ResourceKey + CHAR(10) + 'OldValue=' + @OldResourceValue +  CHAR(10) + 'NewValue=' + @ResourceValue
				END
			END
		END

		IF @TranCounter = 0
			COMMIT TRAN
	END TRY
	BEGIN CATCH
		-- An error occurred; must determine
        -- which type of rollback will roll
        -- back only the work done in the
        -- procedure.
        IF @TranCounter = 0
            -- Transaction started in procedure.
            -- Roll back complete transaction.
            ROLLBACK TRANSACTION;
        ELSE
            -- Transaction started before procedure
            -- called, do not roll back modifications
            -- made before the procedure was called.
            IF XACT_STATE() <> -1
                -- If the transaction is still valid, just
                -- roll back to the savepoint set at the
                -- start of the stored procedure.
                ROLLBACK TRANSACTION main;
                -- If the transaction is uncommitable, a
                -- rollback to the savepoint is not allowed
                -- because the savepoint rollback writes to
                -- the log. Just return to the caller, which
                -- should roll back the outer transaction.
                
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH


GO
