USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegAssUtilizadorEstado]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegAssUtilizadorEstado](
	[IDAssUtilizadorEstado] [bigint] IDENTITY(1,1) NOT NULL,
	[DataInicial] [datetime] NOT NULL,
	[DataFinal] [datetime] NOT NULL CONSTRAINT [DF_TblSegAssUtilizadorEstado_DataFinal]  DEFAULT ('99991231'),
	[IDUtilizador] [bigint] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_TblSegAssUtilizadorEstado] PRIMARY KEY CLUSTERED 
(
	[IDAssUtilizadorEstado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSegAssUtilizadorEstado]  WITH CHECK ADD  CONSTRAINT [FK_TblSegAssUtilizadorEstado_TblSegUtilizador] FOREIGN KEY([IDUtilizador])
REFERENCES [dbo].[TblSegUtilizador] ([IDUtilizador])
GO
ALTER TABLE [dbo].[TblSegAssUtilizadorEstado] CHECK CONSTRAINT [FK_TblSegAssUtilizadorEstado_TblSegUtilizador]
GO
