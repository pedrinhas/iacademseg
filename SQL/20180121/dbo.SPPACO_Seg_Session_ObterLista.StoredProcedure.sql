USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Session_ObterLista]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 2015-02-28
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Session_ObterLista]
	@IDServico int
AS
	SET NOCOUNT ON

	SELECT S.SessionId, S.IDServico, S.IDUtilizador, S.IDUtilizadorHistoricoAcesso, S.Created, S.Expires, S.Timeout, S.LastAccess,
		U.Nome, H.EnderecoIP, H.UserAgent, H.UU,
		W.IDWebserver, W.Hostname
	FROM TblSegSession S
	LEFT JOIN TblSegUtilizador U ON U.IDUtilizador = S.IDUtilizador
	LEFT JOIN TblSegUtilizadorHistoricoAcesso H ON H.IDUtilizadorHistoricoAcesso = S.IDUtilizadorHistoricoAcesso
	LEFT JOIN TblSegWebserver W ON W.IDWebserver = S.IDWebserver
	WHERE S.IDServico = @IDServico



GO
