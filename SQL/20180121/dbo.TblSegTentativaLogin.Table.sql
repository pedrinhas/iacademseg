USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegTentativaLogin]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblSegTentativaLogin](
	[IDTentativa] [bigint] IDENTITY(1,1) NOT NULL,
	[Data] [datetime] NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[EnderecoIP] [char](50) NOT NULL,
 CONSTRAINT [PK_TblSegTentativaLogin] PRIMARY KEY CLUSTERED 
(
	[IDTentativa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
