USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegUtilizadorUltimo]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblSegUtilizadorUltimo](
	[IDSegUtilizadorUltimo] [bigint] IDENTITY(1,1) NOT NULL,
	[IDUtilizador] [bigint] NOT NULL,
	[EnderecoIP] [char](50) NOT NULL,
	[Data] [datetime] NOT NULL,
	[UserAgent] [nvarchar](250) NOT NULL,
	[IDServico] [int] NOT NULL,
	[UU] [nvarchar](100) NULL,
 CONSTRAINT [PK_TblSegUtilizadorUltimo] PRIMARY KEY CLUSTERED 
(
	[IDSegUtilizadorUltimo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TblSegUtilizadorUltimo]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizadorUltimo_IDServico] FOREIGN KEY([IDServico])
REFERENCES [dbo].[TblSegServico] ([IDServico])
GO
ALTER TABLE [dbo].[TblSegUtilizadorUltimo] CHECK CONSTRAINT [FK_TblSegUtilizadorUltimo_IDServico]
GO
ALTER TABLE [dbo].[TblSegUtilizadorUltimo]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizadorUltimo_TblSegUtilizador] FOREIGN KEY([IDUtilizador])
REFERENCES [dbo].[TblSegUtilizador] ([IDUtilizador])
GO
ALTER TABLE [dbo].[TblSegUtilizadorUltimo] CHECK CONSTRAINT [FK_TblSegUtilizadorUltimo_TblSegUtilizador]
GO
