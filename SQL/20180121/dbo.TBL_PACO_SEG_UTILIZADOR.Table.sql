USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TBL_PACO_SEG_UTILIZADOR]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PACO_SEG_UTILIZADOR](
	[IDUtilizador] [bigint] IDENTITY(1,1) NOT NULL,
	[Login] [varchar](500) NOT NULL,
	[Password] [binary](64) NULL,
	[Salt] [uniqueidentifier] NULL,
	[TipoUtilizador] [int] NOT NULL,
	[NumTentativa] [int] NOT NULL,
	[EstadoActual] [int] NOT NULL,
	[Email] [varchar](500) NOT NULL,
 CONSTRAINT [PK__TBL_PACO_SEG_UTI__5165187F] PRIMARY KEY CLUSTERED 
(
	[IDUtilizador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
