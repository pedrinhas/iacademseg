USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblBannersCorGlobal]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblBannersCorGlobal](
	[IDBanner] [int] NOT NULL,
	[IDCorGlobal] [smallint] NOT NULL,
 CONSTRAINT [PK_TblBannersCorGlobal] PRIMARY KEY CLUSTERED 
(
	[IDBanner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblBannersCorGlobal]  WITH CHECK ADD  CONSTRAINT [FK_TblBannersCorGlobal_TblBanners] FOREIGN KEY([IDBanner])
REFERENCES [dbo].[TblBanners] ([IDBanner])
GO
ALTER TABLE [dbo].[TblBannersCorGlobal] CHECK CONSTRAINT [FK_TblBannersCorGlobal_TblBanners]
GO
ALTER TABLE [dbo].[TblBannersCorGlobal]  WITH CHECK ADD  CONSTRAINT [FK_TblBannersCorGlobal_TblSysCorBanners] FOREIGN KEY([IDCorGlobal])
REFERENCES [dbo].[TblSysCorBanners] ([IDCorBanner])
GO
ALTER TABLE [dbo].[TblBannersCorGlobal] CHECK CONSTRAINT [FK_TblBannersCorGlobal_TblSysCorBanners]
GO
