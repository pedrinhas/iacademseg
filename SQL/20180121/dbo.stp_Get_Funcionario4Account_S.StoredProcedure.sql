USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Funcionario4Account_S]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<DescripAtion,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Funcionario4Account_S]
	@numero varchar(50)
	,@email varchar(50) = null -- quando se cria a conta deveria ser fornecido o email para atualizar o erp

AS
BEGIN

declare
@query varchar(3500)
,@numeroIn varchar(50)
,@newid uniqueidentifier
,@seqID bigint 
,@visit bit
,@UU varchar(100)
,@Nome varchar(300)
,@grupo int

set @grupo = 9 --grupo funcionarioUTAD

set @numeroIn=@numero

declare  @temptable table (numero varchar(10),nome varchar(500), e_mail varchar(50), estado int, telefone varchar(255))


-- exec stp_Get_Aluno4Account_S '22578'
	
set @query='SELECT a.emp_num,a.emp_nom,b.email,a.emp_sit,b.emp_tlf_movel FROM sldemp01 a
left JOIN sldemp03 b ON b.emp_num=a.emp_num
 WHERE a.emp_num=''' + @numeroIn + ''''



begin tran
begin try 

	insert into @tempTable
	SELECT a.emp_num,a.emp_nom,b.email,a.emp_sit,b.emp_tlf_movel FROM GIAF..PK.SLDEMP01 a
left JOIN GIAF..PK.SLDEMP03 b ON b.emp_num=a.emp_num
 WHERE a.emp_num=@numeroIn
	--exec(@query) at GIAF

	if exists(select * from @tempTable)
		begin
		if not exists(select numero from [academ_seguranca].[dbo].TblIUPIUtilizador where numero=@numero and idtipoutilizador=0)
			begin
			set @newid=newid()
			set @visit = 0
			--set @UU = 'al' + @numeroIn + '@utad.eu'
			select @UU =  isnull(replace(@email,'@utad.pt',''),'tmp_' + @numeroIn) from @tempTable
			select @email =  isnull(@email,'tmp_' + @numeroIn + '@utad.local') from @tempTable
			select @Nome=nome from @tempTable
		


			exec [academ_seguranca].[dbo].SPPACO_Seg_Utilizador_Adicionar 	@newid,	@Visit, 	@UU,null,@email,@Nome,@grupo,null,@numeroIn

/*
@IUPI uniqueidentifier = null,
	@Visit bit,
	@UU nvarchar(100),
	@Password nvarchar(max) = null,
	@Email nvarchar(100) = @UU,
	@Nome nvarchar(300) = NULL,
	@IDGrupo int,
	@DataLimite datetime = null
*/


				 set @query='update sldemp01 set leg_prov=''' + CAST( @newid as varchar(255)) + '''
					where emp_num=''' + @numeroIn + ''''
				

				exec(@query) at GIAF;
				/*
				if @email is not null
				begin
					set @query='update sldemp03 set email=''' + @email + ''' where emp_num=''' + @numeroIn + ''''
				end

				exec(@query) at GIAF;
*/
				
			end
		end
		
		
				select numero ,nome , @email , estado , telefone from @temptable
		COMMIT tran
end try
begin catch
/*
DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@numeroIn =@numeroIn,
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();	
	        
		  

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
*/
	IF @@TRANCOUNT > 0 ROLLBACK tran;  
SELECT
    ERROR_NUMBER() AS ErrorNumber
    ,@numeroIn as numec
    ,ERROR_SEVERITY() AS ErrorSeverity
    ,ERROR_STATE() AS ErrorState
    ,ERROR_PROCEDURE() AS ErrorProcedure
    ,ERROR_LINE() AS ErrorLine
    ,ERROR_MESSAGE() AS ErrorMessage;

end catch




END




GO
