USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_RegistarAcesso]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 17-06-2011
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_RegistarAcesso]
	@IDUtilizador bigint,
	@IDServico int = NULL,
	@EnderecoIP char(50),
	@Data datetime,
	@UserAgent nvarchar(250),
	@UU nvarchar(100) = NULL
AS
	DECLARE @IDUtilizadorUltimo bigint, @IDUtilizadorHistoricoAcesso bigint
	
	IF @IDServico IS NULL
		SET @IDServico = 1
	
	BEGIN TRY
		BEGIN TRAN
			
		SELECT @IDUtilizadorUltimo = U.IDSegUtilizadorUltimo FROM TblSegUtilizadorUltimo U
					WHERE U.IDUtilizador = @IDUtilizador
		
		IF @IDUtilizadorUltimo IS NULL
		BEGIN
			INSERT INTO TblSegUtilizadorUltimo (IDUtilizador, IDServico, EnderecoIP, Data, UserAgent, UU)
				VALUES (@IDUtilizador, @IDServico, @EnderecoIP, GETDATE(), @UserAgent, @UU)
		END
		ELSE
		BEGIN
			UPDATE TblSegUtilizadorUltimo SET 
				IDServico = @IDServico,
				EnderecoIP = @EnderecoIP,
				Data = GETDATE(),
				UserAgent = @UserAgent,
				UU = @UU
			WHERE IDUtilizador = @IDUtilizador
		END
		
		INSERT INTO TblSegUtilizadorHistoricoAcesso (IDUtilizador, IDServico, EnderecoIP, Data, UserAgent, UU)
				VALUES (@IDUtilizador, @IDServico, @EnderecoIP, GETDATE(), @UserAgent, @UU)

		SET @IDUtilizadorHistoricoAcesso = SCOPE_IDENTITY()

		SELECT @IDUtilizadorHistoricoAcesso AS IDUtilizadorHistoricoAcesso

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();	
	        
		ROLLBACK TRAN

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH


GO
