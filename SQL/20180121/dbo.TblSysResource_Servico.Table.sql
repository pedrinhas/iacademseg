USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSysResource_Servico]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSysResource_Servico](
	[IDResourceServico] [int] IDENTITY(1,1) NOT NULL,
	[IDResourceKey] [int] NOT NULL,
	[IDServico] [int] NOT NULL,
 CONSTRAINT [PK_TblSysResource_Servico] PRIMARY KEY CLUSTERED 
(
	[IDResourceServico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSysResource_Servico]  WITH CHECK ADD  CONSTRAINT [FK_TblSysResource_Servico_IDResourceKey] FOREIGN KEY([IDResourceKey])
REFERENCES [dbo].[TblSysResourceKey] ([IDResourceKey])
GO
ALTER TABLE [dbo].[TblSysResource_Servico] CHECK CONSTRAINT [FK_TblSysResource_Servico_IDResourceKey]
GO
ALTER TABLE [dbo].[TblSysResource_Servico]  WITH CHECK ADD  CONSTRAINT [FK_TblSysResource_Servico_IDServico] FOREIGN KEY([IDServico])
REFERENCES [dbo].[TblSegServico] ([IDServico])
GO
ALTER TABLE [dbo].[TblSysResource_Servico] CHECK CONSTRAINT [FK_TblSysResource_Servico_IDServico]
GO
