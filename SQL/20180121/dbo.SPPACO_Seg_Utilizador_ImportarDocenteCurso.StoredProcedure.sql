USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_ImportarDocenteCurso]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 17-06-2011
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_ImportarDocenteCurso]
	@IUPI uniqueidentifier,
	@Visit bit,
	@UU nvarchar(100) = NULL,
	@Nome nvarchar(300) = NULL
AS
	--DECLARE @IUPI uniqueidentifier = 'ede6a4c4-b271-41fe-a6e7-fdc09fd6a807'
	--DECLARE @IUPI uniqueidentifier = '9f289be5-81aa-4959-95ed-7f0aba18b884'
	--DECLARE @IUPI uniqueidentifier = '5b15a21a-dc0d-4d19-a34f-fef03d65ed80'
	--DECLARE @IUPI uniqueidentifier = 'ed200a40-1e0d-49f8-94fb-7676814076e2'
	
	
	DECLARE @IDGrupoDC smallint = 2
	
	DECLARE @IDUtilizador bigint,
			--@Nome nvarchar(255),
			@IUPIStr varchar(50),
			@NumFuncionario int
	
	SET @IUPIStr = CONVERT(varchar(50), @IUPI)
	
	--DECLARE @Email varchar(256),
	--		@IDUtilizadorAcadem bigint,
	--		@DirectorCurso bit = 0

	
	BEGIN TRY
		BEGIN TRAN
		
			
		SELECT TOP 1 @IDUtilizador = IDUtilizador
		FROM TBL_PACO_SEG_UTILIZADOR U
		WHERE U.Password = @IUPIStr
		
		--IF @Email IS NULL
		--BEGIN
		--	RAISERROR('Não foi possível obter o Email do utilizador. IUPI=%s', 16, 1, @IUPIStr)
		--	RETURN
		--END
		
		--IF @IDUtilizador IS NULL
		--BEGIN
		--	RAISERROR('Não foi possível obter o IDUtilizador do utilizador. IUPI=%s', 16, 1, @IUPIStr)
		--	RETURN
		--END
		
		--SELECT TOP 1 @IDUtilizadorAcadem = DU.RefIDUtilizador, @Nome = D.NomeDoc, @NMecDoc = D.NMecDoc FROM academ_seguranca.dbo.TblDocente D
		--INNER JOIN academ_seguranca.dbo.TblAssDocenteUtilizador DU ON DU.RefIDDocente = D.NMecDoc
		--WHERE D.Email = @Email
		
		--IF @IDUtilizadorAcadem IS NULL
		--BEGIN
		--	RAISERROR('Não foi possível obter o IDUtilizador do docente. Email=%s', 16, 1, @Email)
		--	RETURN
		--END
		
		IF @IDUtilizador IS NULL
		BEGIN
			SELECT TOP 1 @IDUtilizador = IDUtilizador
			FROM TblSegUtilizadorIUPI I 
			WHERE I.IUPI = @IUPI
		END
		
		SELECT TOP 1 @NumFuncionario = IU.Numero FROM academ_seguranca.dbo.TblIUPIUtilizador IU 
			--INNER JOIN academ_seguranca.dbo.TblDocente D ON D.NMecDoc = IU.Numero
			INNER JOIN academ_seguranca.dbo.TblDocenteDirectorCursoINFO DCI ON DCI.RefIDDocente = IU.Numero
			INNER JOIN academ_seguranca.dbo.TblCursoDescricao CD ON CD.CodCurso = DCI.RefIDCurso
			WHERE --(DCI.edicao = 0 OR DCI.edicao IS NULL) AND 
				--DCI.cargo = 1 AND 
				IU.IUPI = @IUPI AND IU.IDTipoUtilizador = 3
		
		IF @NumFuncionario IS NULL
		BEGIN
			RAISERROR('Não existe director de curso com o IUPI=%s', 16, 1, @IUPIStr)
			RETURN
		END
		
		IF @IDUtilizador IS NULL
		BEGIN
			INSERT INTO TBL_PACO_SEG_UTILIZADOR (Login, Password, TipoUtilizador, NumTentativa, EstadoActual, Email)
				VALUES (@UU, @IUPIStr, 5, 0, 1, @UU)
			
			SET @IDUtilizador = SCOPE_IDENTITY()
			
			INSERT INTO TBL_PACO_SEG_UTILIZADOR_REGISTADO (IDUtilizador, NomeUtilizadorRegistado)
				VALUES (@IDUtilizador, @Nome)
		END

		IF NOT EXISTS (SELECT * FROM TblSegUtilizador U WHERE U.IDUtilizador = @IDUtilizador)
		BEGIN
			INSERT INTO TblSegUtilizador (IDUtilizador, Nome)
				VALUES (@IDUtilizador, @Nome)
			
			INSERT INTO TblSegUtilizadorIUPI (IDUtilizador, IUPI, Visit)
				VALUES (@IDUtilizador, @IUPI, @Visit)
			
			INSERT INTO TblSegAssUtilizadorGrupo (IDUtilizador, IDGrupo, DataInicial, DataFinal)
				VALUES (@IDUtilizador, @IDGrupoDC, GETDATE(), CONVERT(datetime, '31-12-9999', 105))
			
			INSERT INTO TblSegAssUtilizadorEstado (IDUtilizador, DataInicial, DataFinal, Activo)
				VALUES (@IDUtilizador, GETDATE(), CONVERT(datetime, '31-12-9999', 105), 1)
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT * FROM TblSegUtilizadorIUPI I WHERE I.IUPI = @IUPI)
			BEGIN
				INSERT INTO TblSegUtilizadorIUPI (IDUtilizador, IUPI, Visit)
					VALUES (@IDUtilizador, @IUPI, @Visit)
			END
			
			IF NOT EXISTS (SELECT * FROM TblSegAssUtilizadorGrupo UG WHERE UG.IDUtilizador = @IDUtilizador AND UG.IDGrupo = @IDGrupoDC 
				AND UG.DataInicial <= GETDATE() AND UG.DataFinal >= GETDATE())
			BEGIN
				INSERT INTO TblSegAssUtilizadorGrupo (IDUtilizador, IDGrupo, DataInicial, DataFinal)
					VALUES (@IDUtilizador, @IDGrupoDC, GETDATE(), CONVERT(datetime, '31-12-9999', 105))
			END
			
			IF NOT EXISTS (SELECT * FROM TblSegAssUtilizadorEstado UE WHERE UE.IDUtilizador = @IDUtilizador AND UE.Activo = 1
				AND UE.DataInicial <= GETDATE() AND UE.DataFinal >= GETDATE())
			BEGIN
				INSERT INTO TblSegAssUtilizadorEstado (IDUtilizador, DataInicial, DataFinal, Activo)
					VALUES (@IDUtilizador, GETDATE(), CONVERT(datetime, '31-12-9999', 105), 1)
			END
		END
		
		IF NOT EXISTS (SELECT * FROM academ_seguranca.dbo.Tbl_PACO_Utilizador U WHERE U.IDUtilizador = @IDUtilizador)
		BEGIN
			INSERT INTO academ_seguranca.dbo.Tbl_PACO_Utilizador (IDUtilizador, Nome, Email, NumFuncionario)
				VALUES (@IDUtilizador, @Nome, @UU, @NumFuncionario)
		END
		ELSE
		BEGIN
			UPDATE academ_seguranca.dbo.Tbl_PACO_Utilizador SET
				Nome = @Nome,
				Email = @UU,
				NumFuncionario = @NumFuncionario
			WHERE IDUtilizador = @IDUtilizador
		END

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();	
	        
		ROLLBACK TRAN

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH


GO
