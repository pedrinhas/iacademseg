USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegServico]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegServico](
	[IDServico] [int] IDENTITY(1,1) NOT NULL,
	[NomeServico] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_TblSegServico] PRIMARY KEY CLUSTERED 
(
	[IDServico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
