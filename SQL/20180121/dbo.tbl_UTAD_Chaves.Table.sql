USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[tbl_UTAD_Chaves]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_UTAD_Chaves](
	[idUtilizador] [int] NOT NULL,
	[ORCID] [varchar](50) NULL,
	[degois] [varchar](50) NULL,
 CONSTRAINT [PK_tbl_UTAD_Chaves] PRIMARY KEY CLUSTERED 
(
	[idUtilizador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
