USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Session_ObterASPState]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 2015-03-05
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Session_ObterASPState]
	@SessionId nvarchar(88),
	@IDServico int
AS
	SET NOCOUNT ON

	--DECLARE @SessionId nvarchar(88) = ''
	--SELECT TOP 0 CONVERT(nvarchar(88), NULL) AS SessionId, CONVERT(varbinary(max), NULL) AS SessionItem
	IF @@SERVERNAME = 'SQL2K8-APPBO\MSSQLAPPBO'
	BEGIN
		SELECT TS.SessionId, CASE WHEN TS.SessionItemShort IS NULL THEN TS.SessionItemLong ELSE TS.SessionItemShort END AS SessionItem
		FROM [web-ia-sessionstate-v4].dbo.ASPStateTempSessions TS 
		WHERE TS.SessionId = @SessionId
	END
	ELSE IF @@SERVERNAME = 'ANALYSIS\MSSQLANALYSIS1'
	BEGIN
		SELECT TS.SessionId, CASE WHEN TS.SessionItemShort IS NULL THEN TS.SessionItemLong ELSE TS.SessionItemShort END AS SessionItem
		FROM [Sessionstate_inst_v4].dbo.ASPStateTempSessions TS 
		WHERE TS.SessionId = @SessionId
	END
	ELSE
	BEGIN
		SELECT TOP 0 CONVERT(nvarchar(88), NULL) AS SessionId, CONVERT(varbinary(max), NULL) AS SessionItem
	END
	


GO
