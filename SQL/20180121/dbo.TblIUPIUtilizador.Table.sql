USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblIUPIUtilizador]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblIUPIUtilizador](
	[IUPI] [varchar](50) NOT NULL,
	[Numero] [varchar](50) NOT NULL,
	[IDTipoUtilizador] [int] NOT NULL,
	[Activo] [bit] NULL,
	[Data] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
