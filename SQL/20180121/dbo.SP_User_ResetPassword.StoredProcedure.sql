USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SP_User_ResetPassword]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
declare @pw nvarchar(8)
declare @usr nvarchar(max)

set @usr = 'den2mee2@gmail.com'

exec [dbo].[SP_User_ResetPassword] @usr,  @pw out

select @pw

*/

CREATE procedure [dbo].[SP_User_ResetPassword]
	@login nvarchar(max),
	@password nvarchar(8) out
as
	declare @newSalt uniqueidentifier,
			@pwdBytes binary(64)

	set @newSalt = newid()

	exec SP_GeneratePassword @password OUTPUT

	exec SP_User_ModifyPassword @login, @password

GO
