USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SP_GetIUPIFuncionario]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		C
-- Create date: 15 jun 2016
-- Description:	Get iupi func a partir do login
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetIUPIFuncionario] 
	@login varchar(50)
AS
BEGIN
	SELECT iu.IUPI
	FROM [dbo].[TBL_PACO_SEG_UTILIZADOR] u
	LEFT JOIN [dbo].[TblSegUtilizadorIUPI] iu on iu.IDUtilizador = u.IDUtilizador
	WHERE u.Login = @login
END

GO
