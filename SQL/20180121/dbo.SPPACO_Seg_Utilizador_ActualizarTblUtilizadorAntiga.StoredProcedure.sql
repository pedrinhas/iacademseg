USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Utilizador_ActualizarTblUtilizadorAntiga]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 16-11-2012
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Utilizador_ActualizarTblUtilizadorAntiga]
	@Login varchar(100),
	@Pwd nvarchar(100)
AS
	BEGIN TRY
		-- Verificar se o iupi existe em TblSegUtilizadorIUPI dado que a TBL_SEG_PACO_UTILIZADOR apenas suporta um IUPI por utilizador
		-- caso exista e seja diferente atualiza a tabela com o IUPI pedido

		--print @Login
		--print @Pwd

		DECLARE @IUPI uniqueidentifier = CONVERT(uniqueidentifier, @Pwd)
		DECLARE @IDUtilizadorTemp bigint

		--print @IUPI

		SELECT TOP 1 @IDUtilizadorTemp = IDUtilizador FROM TblSegUtilizadorIUPI I WHERE I.IUPI = @IUPI
		
		--print @IDUtilizadorTemp

		IF @IDUtilizadorTemp IS NOT NULL AND NOT EXISTS (SELECT * FROM TBL_PACO_SEG_UTILIZADOR U 
						WHERE U.IDUtilizador = @IDUtilizadorTemp AND U.Password LIKE @IUPI AND U.Login LIKE @Login)
		BEGIN
			UPDATE TOP (1) TBL_PACO_SEG_UTILIZADOR SET Login = @Login, Email = @Login, Password = @IUPI WHERE IDUtilizador = @IDUtilizadorTemp
		END

	END TRY
	BEGIN CATCH
	END CATCH


GO
