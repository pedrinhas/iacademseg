USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegUtilizador]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegUtilizador](
	[IDUtilizador] [bigint] NOT NULL,
	[Nome] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_TblSegUtilizador] PRIMARY KEY CLUSTERED 
(
	[IDUtilizador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSegUtilizador]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizador_TblSegUtilizador] FOREIGN KEY([IDUtilizador])
REFERENCES [dbo].[TBL_PACO_SEG_UTILIZADOR] ([IDUtilizador])
GO
ALTER TABLE [dbo].[TblSegUtilizador] CHECK CONSTRAINT [FK_TblSegUtilizador_TblSegUtilizador]
GO
