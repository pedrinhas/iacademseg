USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblBanners]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblBanners](
	[IDBanner] [int] IDENTITY(1,1) NOT NULL,
	[IDServico] [smallint] NOT NULL,
	[IDSubServico] [smallint] NOT NULL,
	[IDTextLeft] [smallint] NOT NULL,
	[IDTextRight] [smallint] NOT NULL,
	[IDDefaultURL] [smallint] NOT NULL,
	[IDAvatar] [smallint] NOT NULL,
	[DescBanner] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_TblBanners] PRIMARY KEY CLUSTERED 
(
	[IDBanner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblBanners]  WITH CHECK ADD  CONSTRAINT [FK_TblBanners_TblBannersDefaultURL] FOREIGN KEY([IDDefaultURL])
REFERENCES [dbo].[TblBannersDefaultURL] ([IDDefaultURL])
GO
ALTER TABLE [dbo].[TblBanners] CHECK CONSTRAINT [FK_TblBanners_TblBannersDefaultURL]
GO
