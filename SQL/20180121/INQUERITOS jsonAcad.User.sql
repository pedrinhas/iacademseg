USE [academ_seguranca]
GO
/****** Object:  User [INQUERITOS\jsonAcad]    Script Date: 23/01/2018 10:42:32 AM ******/
CREATE USER [INQUERITOS\jsonAcad] FOR LOGIN [INQUERITOS\jsonAcad] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_executor] ADD MEMBER [INQUERITOS\jsonAcad]
GO
ALTER ROLE [db_datareader] ADD MEMBER [INQUERITOS\jsonAcad]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [INQUERITOS\jsonAcad]
GO
