USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[createFuncionarioIUPI]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[createFuncionarioIUPI]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- exec [createFuncionarioIUPI]

    -- Insert statements for procedure here
declare
@numero varchar(50)	
,@email varchar(50)
	
DECLARE Aluno_Cursor CURSOR FOR
SELECT DISTINCT A.EMP_NUM,B.EMAIL 
FROM GIAF..PK.SLDEMP01 A 
LEFT JOIN GIAF..PK.SLDEMP03 B ON B.EMP_NUM=A.EMP_NUM
WHERE A.EMP_NUM IN (select DISTINCT EMP_NUM from GIAF..PK.SLHSALARIO WHERE MES=5 AND ANO=2016 ) and a.emp_num='001714'
OPEN Aluno_Cursor;
FETCH NEXT FROM Aluno_Cursor into @numero,@email;
WHILE @@FETCH_STATUS = 0
   BEGIN
   EXEC dbo.stp_Get_Funcionario4Account_S @numero  ,@email
      FETCH NEXT FROM Aluno_Cursor into @numero,@email;
   END;
CLOSE Aluno_Cursor;
DEALLOCATE Aluno_Cursor;

	
	
	
END

GO
