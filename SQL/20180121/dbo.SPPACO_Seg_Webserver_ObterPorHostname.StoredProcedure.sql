USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SPPACO_Seg_Webserver_ObterPorHostname]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		João Taborda
-- Create date: 2015-02-28
-- Description:	
-- Changes:
-- =============================================
CREATE PROCEDURE [dbo].[SPPACO_Seg_Webserver_ObterPorHostname]
	@Hostname nvarchar(50)
AS
	SET NOCOUNT ON

	SELECT W.IDWebserver, W.Hostname, W.Created
	FROM TblSegWebserver W
	WHERE W.Hostname = @Hostname



GO
