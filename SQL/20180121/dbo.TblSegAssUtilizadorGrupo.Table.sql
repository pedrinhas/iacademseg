USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegAssUtilizadorGrupo]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegAssUtilizadorGrupo](
	[IDAssUtilizadorGrupo] [bigint] IDENTITY(1,1) NOT NULL,
	[DataInicial] [datetime] NOT NULL,
	[DataFinal] [datetime] NOT NULL CONSTRAINT [DF_TblSegAssUtilizadorGrupo_DataFinal]  DEFAULT ('99991231'),
	[IDUtilizador] [bigint] NOT NULL,
	[IDGrupo] [int] NOT NULL,
 CONSTRAINT [PK_TblSegAssUtilizadorGrupo] PRIMARY KEY CLUSTERED 
(
	[IDAssUtilizadorGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSegAssUtilizadorGrupo]  WITH CHECK ADD  CONSTRAINT [FK_TblSegAssUtilizadorGrupo_TblSegGrupo] FOREIGN KEY([IDGrupo])
REFERENCES [dbo].[TblSegGrupo] ([IDGrupo])
GO
ALTER TABLE [dbo].[TblSegAssUtilizadorGrupo] CHECK CONSTRAINT [FK_TblSegAssUtilizadorGrupo_TblSegGrupo]
GO
ALTER TABLE [dbo].[TblSegAssUtilizadorGrupo]  WITH CHECK ADD  CONSTRAINT [FK_TblSegAssUtilizadorGrupo_TblSegUtilizador] FOREIGN KEY([IDUtilizador])
REFERENCES [dbo].[TblSegUtilizador] ([IDUtilizador])
GO
ALTER TABLE [dbo].[TblSegAssUtilizadorGrupo] CHECK CONSTRAINT [FK_TblSegAssUtilizadorGrupo_TblSegUtilizador]
GO
