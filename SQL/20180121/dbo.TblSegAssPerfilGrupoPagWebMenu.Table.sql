USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegAssPerfilGrupoPagWebMenu]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegAssPerfilGrupoPagWebMenu](
	[IDPerfilGrupo] [smallint] NOT NULL,
	[IDPaginaWeb] [int] NOT NULL,
	[IDMenu] [int] NOT NULL,
 CONSTRAINT [PK_TblSegAssPerfilGrupoPagWebMenu] PRIMARY KEY CLUSTERED 
(
	[IDPerfilGrupo] ASC,
	[IDPaginaWeb] ASC,
	[IDMenu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSegAssPerfilGrupoPagWebMenu]  WITH CHECK ADD  CONSTRAINT [FK_TblSegAssPerfilGrupoPagWebMenu_TblSegMenu] FOREIGN KEY([IDMenu])
REFERENCES [dbo].[TblSegMenu] ([IDMenu])
GO
ALTER TABLE [dbo].[TblSegAssPerfilGrupoPagWebMenu] CHECK CONSTRAINT [FK_TblSegAssPerfilGrupoPagWebMenu_TblSegMenu]
GO
ALTER TABLE [dbo].[TblSegAssPerfilGrupoPagWebMenu]  WITH CHECK ADD  CONSTRAINT [FK_TblSegAssPerfilGrupoPagWebMenu_TblSegPaginaWeb] FOREIGN KEY([IDPaginaWeb])
REFERENCES [dbo].[TblSegPaginaWeb] ([IDPaginaWeb])
GO
ALTER TABLE [dbo].[TblSegAssPerfilGrupoPagWebMenu] CHECK CONSTRAINT [FK_TblSegAssPerfilGrupoPagWebMenu_TblSegPaginaWeb]
GO
ALTER TABLE [dbo].[TblSegAssPerfilGrupoPagWebMenu]  WITH CHECK ADD  CONSTRAINT [FK_TblSegAssPerfilGrupoPagWebMenu_TblSegPerfilGrupo] FOREIGN KEY([IDPerfilGrupo])
REFERENCES [dbo].[TblSegPerfilGrupo] ([IDPerfilGrupo])
GO
ALTER TABLE [dbo].[TblSegAssPerfilGrupoPagWebMenu] CHECK CONSTRAINT [FK_TblSegAssPerfilGrupoPagWebMenu_TblSegPerfilGrupo]
GO
