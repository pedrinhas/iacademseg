USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSysResourceKey]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblSysResourceKey](
	[IDResourceKey] [int] IDENTITY(1,1) NOT NULL,
	[ResourceType] [varchar](100) NOT NULL,
	[ResourceKey] [varchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_TblSysResourceKey] PRIMARY KEY CLUSTERED 
(
	[IDResourceKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
