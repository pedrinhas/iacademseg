USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegUtilizadorIUPI]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegUtilizadorIUPI](
	[IDUtilizadorIUPI] [bigint] IDENTITY(1,1) NOT NULL,
	[IDUtilizador] [bigint] NOT NULL,
	[IUPI] [uniqueidentifier] NOT NULL,
	[DataAtribuicao] [datetime] NOT NULL DEFAULT (getdate()),
	[Visit] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_TblSegUtilizadorIUPI] PRIMARY KEY CLUSTERED 
(
	[IDUtilizadorIUPI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSegUtilizadorIUPI]  WITH CHECK ADD  CONSTRAINT [FK_TblSegUtilizadorIUPI_TblSegUtilizador] FOREIGN KEY([IDUtilizador])
REFERENCES [dbo].[TblSegUtilizador] ([IDUtilizador])
GO
ALTER TABLE [dbo].[TblSegUtilizadorIUPI] CHECK CONSTRAINT [FK_TblSegUtilizadorIUPI_TblSegUtilizador]
GO
