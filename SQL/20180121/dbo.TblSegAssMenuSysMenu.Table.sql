USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegAssMenuSysMenu]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegAssMenuSysMenu](
	[IDMenu] [int] NOT NULL,
	[MenuID] [int] NOT NULL,
	[Data] [datetime] NOT NULL,
	[IDUtilizador] [bigint] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_TblSegAssMenuSysMenu] PRIMARY KEY CLUSTERED 
(
	[IDMenu] ASC,
	[MenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSegAssMenuSysMenu]  WITH CHECK ADD  CONSTRAINT [FK_TblSegAssMenuSysMenu_TblSegMenu] FOREIGN KEY([IDMenu])
REFERENCES [dbo].[TblSegMenu] ([IDMenu])
GO
ALTER TABLE [dbo].[TblSegAssMenuSysMenu] CHECK CONSTRAINT [FK_TblSegAssMenuSysMenu_TblSegMenu]
GO
ALTER TABLE [dbo].[TblSegAssMenuSysMenu]  WITH CHECK ADD  CONSTRAINT [FK_TblSegAssMenuSysMenu_TblSysMenu] FOREIGN KEY([MenuID])
REFERENCES [dbo].[TblSysMenu] ([MenuId])
GO
ALTER TABLE [dbo].[TblSegAssMenuSysMenu] CHECK CONSTRAINT [FK_TblSegAssMenuSysMenu_TblSysMenu]
GO
