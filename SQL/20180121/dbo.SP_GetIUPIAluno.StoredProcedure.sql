USE [academ_seguranca]
GO
/****** Object:  StoredProcedure [dbo].[SP_GetIUPIAluno]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		C
-- Create date: 15 jun 2016
-- Description:	Get iupi a partir do numero
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetIUPIAluno] 
	@numero varchar(50)
AS
BEGIN
	SELECT iu.IUPI
	FROM [dbo].[Tbl_PACO_Utilizador] u
	LEFT JOIN [dbo].[TblSegUtilizadorIUPI] iu on iu.IDUtilizador = u.IDUtilizador
	WHERE u.NumAluno = @numero
END

GO
