USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSysMenu]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSysMenu](
	[MenuId] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [nvarchar](100) NOT NULL,
	[PaiId] [int] NOT NULL,
	[Posicao] [smallint] NOT NULL,
	[Icon] [bit] NOT NULL,
	[Habilitado] [bit] NOT NULL,
	[IDPaginaWeb] [int] NULL,
	[Url] [nvarchar](200) NULL,
	[DataCriacao] [smalldatetime] NOT NULL,
	[UserCriacao] [bigint] NOT NULL,
	[DataModificacao] [smalldatetime] NULL,
	[UserModificacao] [bigint] NULL,
 CONSTRAINT [TblSysMenu_PK] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSysMenu] ADD  CONSTRAINT [DF_TblSysMenu_Icon]  DEFAULT ((0)) FOR [Icon]
GO
ALTER TABLE [dbo].[TblSysMenu] ADD  CONSTRAINT [DF_TblSysMenu_DataCriacao]  DEFAULT (getdate()) FOR [DataCriacao]
GO
ALTER TABLE [dbo].[TblSysMenu] ADD  CONSTRAINT [DF_TblSysMenu_DataModificacao]  DEFAULT (getdate()) FOR [DataModificacao]
GO
ALTER TABLE [dbo].[TblSysMenu]  WITH CHECK ADD  CONSTRAINT [FK_TblSysMenu_TblSegPaginaWeb] FOREIGN KEY([IDPaginaWeb])
REFERENCES [dbo].[TblSegPaginaWeb] ([IDPaginaWeb])
GO
ALTER TABLE [dbo].[TblSysMenu] CHECK CONSTRAINT [FK_TblSysMenu_TblSegPaginaWeb]
GO
ALTER TABLE [dbo].[TblSysMenu]  WITH CHECK ADD  CONSTRAINT [FK_TblSysMenu_TblSegUtilizador] FOREIGN KEY([UserCriacao])
REFERENCES [dbo].[TblSegUtilizador] ([IDUtilizador])
GO
ALTER TABLE [dbo].[TblSysMenu] CHECK CONSTRAINT [FK_TblSysMenu_TblSegUtilizador]
GO
ALTER TABLE [dbo].[TblSysMenu]  WITH CHECK ADD  CONSTRAINT [FK_TblSysMenu_TblSegUtilizador1] FOREIGN KEY([UserModificacao])
REFERENCES [dbo].[TblSegUtilizador] ([IDUtilizador])
GO
ALTER TABLE [dbo].[TblSysMenu] CHECK CONSTRAINT [FK_TblSysMenu_TblSegUtilizador1]
GO
ALTER TABLE [dbo].[TblSysMenu]  WITH CHECK ADD  CONSTRAINT [FK_TblSysMenu_TblSysMenu] FOREIGN KEY([PaiId])
REFERENCES [dbo].[TblSysMenu] ([MenuId])
GO
ALTER TABLE [dbo].[TblSysMenu] CHECK CONSTRAINT [FK_TblSysMenu_TblSysMenu]
GO
