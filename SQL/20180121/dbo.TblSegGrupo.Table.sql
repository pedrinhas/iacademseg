USE [academ_seguranca]
GO
/****** Object:  Table [dbo].[TblSegGrupo]    Script Date: 23/01/2018 10:42:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSegGrupo](
	[IDGrupo] [int] IDENTITY(1,1) NOT NULL,
	[NomeGrupo] [nvarchar](150) NOT NULL,
	[IDPerfilGrupo] [smallint] NOT NULL,
	[TimeOut] [smallint] NOT NULL,
 CONSTRAINT [PK_TblSegGrupo] PRIMARY KEY CLUSTERED 
(
	[IDGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TblSegGrupo]  WITH CHECK ADD  CONSTRAINT [FK_TblSegGrupo_TblSegPerfilGrupo] FOREIGN KEY([IDPerfilGrupo])
REFERENCES [dbo].[TblSegPerfilGrupo] ([IDPerfilGrupo])
GO
ALTER TABLE [dbo].[TblSegGrupo] CHECK CONSTRAINT [FK_TblSegGrupo_TblSegPerfilGrupo]
GO
