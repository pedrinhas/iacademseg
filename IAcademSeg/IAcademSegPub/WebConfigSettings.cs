﻿using ErrorHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace IAcademSegPub
{

    public static class WebConfigSettings
    {
        public static bool Debug { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((ConfigurationManager.AppSettings["Debug"] ?? "False").ToLowerInvariant()); } }

        public static class ConnectionStrings
        {
            private static string getConnString(string name)
            {
                try
                {
                    if (ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>().Select(x => x.Name).Contains(name))
                    {
                        return ConfigurationManager.ConnectionStrings[name].ConnectionString;
                    }
                    else return "";
                }
                catch (Exception ex)
                {
                    var e = new Exception(string.Format("A connection string {0} não existe no Web.config", name), ex);
                    throw e;
                }
            }
            public static string AcademSeguranca { get { return getConnString("academ_seguranca"); } }
        }
        public static class AppSettings
        {
            private static string getValue(string key)
            {
                try
                {
                    if (ConfigurationManager.AppSettings.Cast<string>().Contains(key))
                    {
                        return ConfigurationManager.AppSettings[key];
                    }
                    else return "";
                }
                catch (Exception ex)
                {
                    var e = new Exception(string.Format("A key {0} não existe no Web.config", key), ex);

                    ErrorHandling.LogError(e, ErrorHandlingEnums.Severity.Critical);
                    return "";
                }
            }
            public static class WebService
            {
                public static string Name { get { return getValue("WebService.Name"); } }
            }
            public static class Email
            {
                public static string SMTPServer
                {
                    get
                    {
                        return ConfigurationManager.AppSettings["Email.STMPServer"].ToString();
                    }
                }
                public static class ErrorLog
                {
                    public static bool Log { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("Email.LogByEmail") ?? "False").ToLowerInvariant()); } }
                    public static string From
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.From");
                        }
                    }
                    public static List<string> To
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.To").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                    }
                    public static List<string> CC
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.CC").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                    }
                    public static List<string> BCC
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.BCC").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                    }
                }

                public static string From
                {
                    get
                    {
                        return getValue("Email.From");
                    }
                }
                public static List<string> CC
                {
                    get
                    {
                        return getValue("Email.CC").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    }
                }
                public static List<string> BCC
                {
                    get
                    {
                        return getValue("Email.BCC").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    }
                }
            }
            public static class Errors
            {
                public static int LogLevel
                {
                    get
                    {
                        try
                        {
                            int level;

                            if (int.TryParse(getValue("Errors.LogLevel"), out level))
                            {
                                return level;
                            }
                        }
                        catch (Exception ex)
                        {
                            var e = new ArgumentException("Não foi possível interpretar o LogLevel no Web.config", ex);
                            ErrorHandling.LogError(e, ErrorHandlingEnums.Severity.Critical);
                        }


                        //Warning não deve ser muito verbose, mas manda os erros para trás
                        return 1;
                    }
                }
            }
            public static class Authentication
            {
                public static string[] AllowedAllAccessIPs { get { return UseAllowedAllAccessIPWhiteList ? (getValue("Authentication.AllowedAllAccessIPs") + ",::1,localhost,127.0.0.1").Split(new char[] { ',', ';', '-', ' ', '|' }) : new string[0]; } }
                public static bool UseAllowedAllAccessIPWhiteList { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("Authentication.UseAllowedAllAccessIPWhiteList") ?? "False").ToLowerInvariant()); } }
            }
            public static class Redirects
            {
                public static string PasswordResetSuccess { get { return getValue("Redirects.PasswordResetSuccess"); } }
                public static string PasswordResetError { get { return getValue("Redirects.PasswordResetError"); } }
            }
        }
    }
}