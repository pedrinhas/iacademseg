﻿using ErrorHelper;
using IAcademSeg;
using IAcademSeg.Returns;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Web;

namespace IAcademSegPub
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AcadSegPub : IAcadSegPub
    {
        public void ResetPassword(string username, string token)
        {
            var res = new ResetPasswordReturnValue();

            try
            {
                if (string.IsNullOrWhiteSpace(username))
                {
                    res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "O email não pode ser vazio");
                }
                else if (!(new AcadSeg()).ChallengeToken(username, token))
                {
                    res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "O token fornecido é inválido");
                }
                else
                {
                    var accExists = (new AcadSeg()).AccountExists(username);

                    if (accExists.Success && (accExists as AccountExistsReturnValue).Exists)
                    {
                        using (var con = new SqlConnection(WebConfigSettings.ConnectionStrings.AcademSeguranca))
                        using (var cmd = new SqlCommand())
                        {
                            cmd.CommandText = "SP_User_ResetPassword";
                            cmd.Connection = con;
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            cmd.Parameters.Add("@login", System.Data.SqlDbType.NVarChar).Value = username;
                            cmd.Parameters.Add("@password", System.Data.SqlDbType.NVarChar, 8).Direction = System.Data.ParameterDirection.Output;

                            if (con.State != System.Data.ConnectionState.Open) con.Open();

                            cmd.ExecuteNonQuery();

                            res.NewPassword = cmd.Parameters["@password"].Value.ToString();

                            con.Close();
                        }

                        //var body = string.Format("No acesso à aplicação de Candidaturas a Bolsas de Investigação da Universidade de Trás-os-Montes e Alto Douro, use as seguintes credenciais:{0}{0}Email: {1}{0}Password: {2}{0}{0}Aceda ao seguinte endereço para efetuar login: https://www.campus.utad.pt/bolsasinvestigacao/candidaturas/Session/Login{0}{0}Nesta plataforma poderá acompanhar o processo da sua candidatura, assim como consultar os resultados da sua avaliação quando disponíveis.{0}Mensagem emitida AUTOMATICAMENTE pelo Sistema Informático do Núcleo de Sistema de Informação da Universidade de Trás-os-Montes e Alto Douro", Environment.NewLine, username, res.NewPassword);

                        var body = string.Format("No acesso à aplicação, use as seguintes credenciais:{0}{0}Email: {1}{0}Password: {2}{0}{0}Mensagem emitida automaticamente pelo sistema de informação do Núcleo de Sistemas de Informação da Universidade de Trás-os-Montes e Alto Douro", Environment.NewLine, username, res.NewPassword);

                        var to = new List<string>() { username };

                        Tools.Email.SendEmail(to, "Credenciais de acesso", body, WebConfigSettings.AppSettings.Email.CC, WebConfigSettings.AppSettings.Email.BCC);
                    }
                    else if (accExists.Success)
                    {
                        res.Success = false;
                    }
                    else
                    {
                        throw new Exception("Não foi possível repor a password");
                    }
                }
            }
            catch (Exception ex)
            {
                res.Error = ErrorReturnObject.Create(Guid.NewGuid(), ex.Message);

                ErrorHandling.LogError(ex, res);
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Status = "302 FOUND";

            if(res.Error == null)
            {
                //success
                HttpContext.Current.Response.Redirect(WebConfigSettings.AppSettings.Redirects.PasswordResetSuccess);
            }
            else
            {
                //error
                HttpContext.Current.Response.Redirect(WebConfigSettings.AppSettings.Redirects.PasswordResetError);
            }

            HttpContext.Current.Response.End();
        }
    }
}
