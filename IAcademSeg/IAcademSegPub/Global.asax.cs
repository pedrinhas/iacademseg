﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace IAcademSegPub
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();

            ErrorHelper.ErrorHandling.Initialize(WebConfigSettings.AppSettings.WebService.Name, WebConfigSettings.Debug, WebConfigSettings.AppSettings.Email.SMTPServer, WebConfigSettings.AppSettings.Email.ErrorLog.From, WebConfigSettings.AppSettings.Email.ErrorLog.To, WebConfigSettings.AppSettings.Email.ErrorLog.CC, WebConfigSettings.AppSettings.Email.ErrorLog.BCC, (ErrorHelper.ErrorHandlingEnums.Severity)WebConfigSettings.AppSettings.Errors.LogLevel);
        }

        private void RegisterRoutes()
        {
            RouteTable.Routes.Add(new ServiceRoute("", new WebServiceHostFactory(), typeof(AcadSegPub)));
        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}