﻿using IAcademSeg.Returns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IAcademSegPub
{
    [ServiceContract]
    public interface IAcadSegPub
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ResetPassword?u={username}&t={token}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        void ResetPassword(string username, string token);
    }
}
