﻿using ErrorHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace IAcademSeg
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();

            ErrorHandling.Initialize(WebConfigSettings.AppSettings.WebService.Name, WebConfigSettings.Debug, WebConfigSettings.AppSettings.Email.SMTPServer, WebConfigSettings.AppSettings.Email.ErrorLog.From, WebConfigSettings.AppSettings.Email.ErrorLog.To, WebConfigSettings.AppSettings.Email.ErrorLog.CC, WebConfigSettings.AppSettings.Email.ErrorLog.BCC, (ErrorHandlingEnums.Severity)WebConfigSettings.AppSettings.Errors.LogLevel);
        }

        private void RegisterRoutes()
        {
            RouteTable.Routes.Add(new ServiceRoute("", new WebServiceHostFactory(), typeof(AcadSeg)));
        }
    }
}