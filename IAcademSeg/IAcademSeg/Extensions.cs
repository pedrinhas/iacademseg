﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IAcademSeg
{

    public static class Extensions
    {
        public static string GetString(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return reader.GetString(idCol);
            return null;
        }

        public static DateTime? GetDateTime(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return new DateTime?(reader.GetDateTime(idCol));
            return null;
        }

        public static int? GetInt32(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return reader.GetInt32(idCol);
            return null;
        }

        public static bool? GetBoolean(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return reader.GetBoolean(idCol);
            return null;
        }

        public static Guid? GetGuid(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return reader.GetGuid(idCol);
            return null;
        }

        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            return dr.GetColumnID(columnName) >= 0;
        }

        public static int GetColumnID(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return i;
            }
            return -1;
        }
        public static bool IsDBNull(this IDataRecord dr, string columnName)
        {
            var idCol = dr.GetColumnID(columnName);

            if (idCol > -1) return dr.IsDBNull(idCol);
            return true;
        }

        public static object[] GetAllValues(this IDataRecord dr)
        {
            var all = new object[dr.FieldCount];
            dr.GetValues(all);

            return all;
        }
    }
}