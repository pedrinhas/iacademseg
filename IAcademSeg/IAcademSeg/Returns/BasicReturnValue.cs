﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IAcademSeg.Returns
{
    [DataContract]
    [KnownType(typeof(LoginReturnValue))]
    [KnownType(typeof(RegisterUserReturnValue))]
    [KnownType(typeof(AccountExistsReturnValue))]
    [KnownType(typeof(ResetPasswordReturnValue))]
    [KnownType(typeof(ModifyPasswordReturnValue))]
    public class BasicReturnValue
    {
        public BasicReturnValue()
        {
            Success = true;
        }
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public ErrorReturnObject Error { get; set; }
    }
}