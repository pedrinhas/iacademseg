﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IAcademSeg.Returns
{
    [DataContract]
    public class LoginReturnValue : BasicReturnValue
    {
        public LoginReturnValue()
        {
            LoggedIn = false;
        }
        [DataMember]
        public bool LoggedIn { get; set; }
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public Guid IUPI { get; set; }
    }
}