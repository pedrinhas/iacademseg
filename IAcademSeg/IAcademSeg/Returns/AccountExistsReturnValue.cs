﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IAcademSeg.Returns
{
    [DataContract]
    public class AccountExistsReturnValue : BasicReturnValue
    {
        [DataMember]
        public bool Exists { get; set; }
    }
}