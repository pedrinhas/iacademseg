﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IAcademSeg.Returns
{
    [DataContract]
    public class RegisterUserReturnValue : BasicReturnValue
    {
        [DataMember]
        public string IUPI { get; set; }
    }
}