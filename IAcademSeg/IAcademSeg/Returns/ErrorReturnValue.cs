﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IAcademSeg.Returns
{
    [DataContract]
    public class ErrorReturnObject
    {
        [DataMember]
        public Guid ErrorID { get; set; }
        [DataMember]
        public string Message { get; set; }

        public static ErrorReturnObject Create(Guid err, string msg)
        {
            return new ErrorReturnObject()
            {
                ErrorID = err,
                Message = msg
            };
        }
    }
}