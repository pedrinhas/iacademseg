﻿using IAcademSeg.Returns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IAcademSeg
{
    [ServiceContract]
    public interface IAcadSeg
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "AccountExists?username={username}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        AccountExistsReturnValue AccountExists(string username);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RegisterUser", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        RegisterUserReturnValue RegisterUser(string email, string nome);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DoLogin", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        LoginReturnValue DoLogin(string username, string password);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ModifyPassword", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        ModifyPasswordReturnValue ModifyPassword(string username, string password, string newPassword);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ResetPassword", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        BasicReturnValue ResetPassword(string username);

#if DEBUG
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DeleteAccount", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        BasicReturnValue DeleteAccount(string username);
#endif
    }
}
