﻿using ErrorHelper;
using IAcademSeg.Returns;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ServiceModel.Activation;

namespace IAcademSeg
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AcadSeg : IAcadSeg
    {
        public AccountExistsReturnValue AccountExists(string email)
        {
            var res = new AccountExistsReturnValue()
                {
                    Exists = false
                };

            try
            {
                if (string.IsNullOrWhiteSpace(email))
                {
                    res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "O username não pode ser vazio");
                    res.Success = false;
                }
                else
                {
                    using (SqlConnection con = new SqlConnection(WebConfigSettings.ConnectionStrings.AcademSeguranca))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.CommandText = "SP_User_Exists";
                            cmd.Connection = con;
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            cmd.Parameters.Add("@UU", System.Data.SqlDbType.NVarChar).Value = email;

                            if (con.State != System.Data.ConnectionState.Open) con.Open();

                            SqlDataReader reader = cmd.ExecuteReader();

                            while (reader.Read())
                            {
                                res.Exists = true;
                            }
                        }

                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                res.Error = ErrorReturnObject.Create(Guid.NewGuid(), ex.Message);
                res.Success = false;

                ErrorHandling.LogError(ex, res);
            }

            return res;
        }
        public RegisterUserReturnValue RegisterUser(string email, string nome)
        {
            var res = new RegisterUserReturnValue();
            string uName = "";
            string pwd = "";

            try
            {
                if (string.IsNullOrWhiteSpace(email))
                {
                    res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "O email não pode ser vazio");
                    res.Success = false;
                }
                else
                {
                    var accExists = AccountExists(email);
                    if (accExists.Success && !(accExists as AccountExistsReturnValue).Exists) //Não fazer o cast porque pode ter devolvido erro
                    {
                        using (SqlConnection con = new SqlConnection(WebConfigSettings.ConnectionStrings.AcademSeguranca))
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.CommandText = "SPPACO_Seg_Utilizador_Adicionar";
                                cmd.Connection = con;
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                                cmd.Parameters.Add("@Visit", System.Data.SqlDbType.Bit).Value = true;
                                cmd.Parameters.Add("@UU", System.Data.SqlDbType.NVarChar).Value = email;
                                cmd.Parameters.Add("@Email", System.Data.SqlDbType.NVarChar).Value = email;
                                cmd.Parameters.Add("@Nome", System.Data.SqlDbType.NVarChar).Value = nome;
                                cmd.Parameters.Add("@IDGrupo", System.Data.SqlDbType.Int).Value = 8;

                                cmd.Parameters.Add("@password", System.Data.SqlDbType.NVarChar, 8).Direction = System.Data.ParameterDirection.Output;

                                if (con.State != System.Data.ConnectionState.Open) con.Open();

                                SqlDataReader reader = cmd.ExecuteReader();

                                while (reader.Read())
                                {
                                    uName = reader.GetString("UU");
                                    pwd = reader.GetString("Password");
                                    res.IUPI = reader.GetGuid("IUPI").ToString();
                                }
                            }

                            con.Close();
                        }

                        //var body = string.Format("No acesso à aplicação de Candidaturas a Bolsas de Investigação da Universidade de Trás-os-Montes e Alto Douro, use as seguintes credenciais:{0}{0}Email: {1}{0}Password: {2}{0}{0}Aceda ao seguinte endereço para efetuar login: https://www.campus.utad.pt/bolsasinvestigacao/candidaturas/Session/Login{0}{0}Nesta plataforma poderá acompanhar o processo da sua candidatura, assim como consultar os resultados da sua avaliação quando disponíveis.{0}Mensagem emitida AUTOMATICAMENTE pelo Sistema Informático do Núcleo de Sistema de Informação da Universidade de Trás-os-Montes e Alto Douro", Environment.NewLine, uName, pwd);


                        var body = string.Format("No acesso à aplicação, use as seguintes credenciais:{0}{0}Email: {1}{0}Password: {2}{0}{0}Mensagem emitida automaticamente pelo sistema de informação do Núcleo de Sistemas de Informação da Universidade de Trás-os-Montes e Alto Douro", Environment.NewLine, uName, pwd);
                        var bodyNoPwd = string.Format("No acesso à aplicação, use as seguintes credenciais:{0}{0}Email: {1}{0}Password: XXXXXXXXXX{0}{0}Mensagem emitida automaticamente pelo sistema de informação do Núcleo de Sistemas de Informação da Universidade de Trás-os-Montes e Alto Douro", Environment.NewLine, uName);
                        var to = new List<string>() { email };

                        Tools.Email.SendEmail(to, "Credenciais de acesso", body, WebConfigSettings.AppSettings.Email.CC);
                        Tools.Email.SendEmail(WebConfigSettings.AppSettings.Email.BCC, "Credenciais de acesso", bodyNoPwd, WebConfigSettings.AppSettings.Email.CC);
                    }
                    else
                    {
                        res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "Já existe uma conta com este email");
                        res.Success = false;
                    }
                }
            }
            catch (Exception ex)
            {
                res.Error = ErrorReturnObject.Create(Guid.NewGuid(), ex.Message);
                res.Success = false;

                ErrorHandling.LogError(ex, res);
            }

            return res;
        }
        public LoginReturnValue DoLogin(string email, string password)
        {
            var res = new LoginReturnValue();

            try
            {
                if (string.IsNullOrWhiteSpace(email))
                {
                    res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "O email não pode ser vazio");
                    res.Success = false;
                }
                else
                {
                    var accExists = AccountExists(email);
                    if (accExists.Success && (accExists as AccountExistsReturnValue).Exists) //Não fazer o cast porque pode ter devolvido erro
                    {
                        using (SqlConnection con = new SqlConnection(WebConfigSettings.ConnectionStrings.AcademSeguranca))
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.CommandText = "SP_LocalLogin_DoLogin";
                                cmd.Connection = con;
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                                cmd.Parameters.Add("@username", System.Data.SqlDbType.NVarChar).Value = email;
                                cmd.Parameters.Add("@password", System.Data.SqlDbType.NVarChar).Value = password;

                                if (con.State != System.Data.ConnectionState.Open) con.Open();

                                SqlDataReader reader = cmd.ExecuteReader();

                                if (reader.Read())
                                {
                                    res.LoggedIn = true;
                                    res.ID = Convert.ToString(reader["IDUtilizador"]);
                                    res.Nome = Convert.ToString(reader["Nome"]);
                                    res.Username = Convert.ToString(reader["Login"]);
                                    res.Email = Convert.ToString(reader["Email"]);
                                    res.IUPI = Guid.Parse(Convert.ToString(reader["IUPI"]));
                                }
                                else
                                {
                                    res.LoggedIn = false;
                                }
                            }

                            con.Close();
                        }
                    }
                    else if (accExists.Success)
                    {
                        res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "A conta não existe");
                        res.Success = false;
                    }
                    else
                    {
                        throw new Exception("Não foi possível determinar se a conta existe");
                    }
                }
            }
            catch (Exception ex)
            {
                res.Error = ErrorReturnObject.Create(Guid.NewGuid(), ex.Message);
                res.Success = false;

                ErrorHandling.LogError(ex, res);
            }

            return res;
        }
        public ModifyPasswordReturnValue ModifyPassword(string email, string password, string newPassword)
        {
            var res = new ModifyPasswordReturnValue();

            try
            {
                if (string.IsNullOrWhiteSpace(email))
                {
                    res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "O email não pode ser vazio");
                    res.Success = false;
                }
                else
                {
                    var loginResult = DoLogin(email, password);

                    if (loginResult.Success && (loginResult as LoginReturnValue).LoggedIn)
                    {
                        using (var con = new SqlConnection(WebConfigSettings.ConnectionStrings.AcademSeguranca))
                        using (var cmd = new SqlCommand())
                        {
                            cmd.CommandText = "SP_User_ModifyPassword";
                            cmd.Connection = con;
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            cmd.Parameters.Add("@login", System.Data.SqlDbType.NVarChar).Value = email;
                            cmd.Parameters.Add("@newPassword", System.Data.SqlDbType.NVarChar).Value = newPassword;

                            if (con.State != System.Data.ConnectionState.Open) con.Open();

                            cmd.ExecuteNonQuery();
                        }
                    }
                    else if (loginResult.Success)
                    {
                        res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "Password incorreta");
                        res.Success = false;
                    }
                    else
                    {
                        throw new Exception("Não foi possível autenticar o utilizador");
                    }
                }
            }
            catch (Exception ex)
            {
                res.Error = ErrorReturnObject.Create(Guid.NewGuid(), ex.Message);
                res.Success = false;

                ErrorHandling.LogError(ex, res);
            }

            return res;
        }
        public BasicReturnValue ResetPassword(string email)
        {
            var res = new BasicReturnValue();
            string token = "";
            try
            {
                if (string.IsNullOrWhiteSpace(email))
                {
                    res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "O email não pode ser vazio");
                    res.Success = false;
                }
                else
                {
                    var accExists = AccountExists(email);

                    if (accExists.Success && (accExists as AccountExistsReturnValue).Exists)
                    {
                        using (var con = new SqlConnection(WebConfigSettings.ConnectionStrings.AcademSeguranca))
                        using (var cmd = new SqlCommand())
                        {
                            cmd.CommandText = "[SP_User_ResetPassword_GiveToken]";
                            cmd.Connection = con;
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            cmd.Parameters.Add("@UU", System.Data.SqlDbType.NVarChar).Value = email;

                            if (con.State != System.Data.ConnectionState.Open) con.Open();

                            var reader = cmd.ExecuteReader();

                            if(reader.Read() && reader.GetColumnID("Token") == 0)
                            {
                                token = reader.GetString("Token");
                            }
                        }

                        if(!string.IsNullOrWhiteSpace(token))
                        {
                            var addr = string.Format(WebConfigSettings.AppSettings.Email.ResetPassword.PubEndpointAddressTemplate, email, token);
                            var body = string.Format("Foi efetuado um pedido de reposição de palavra-passe para este endereço de correio eletrónico.{0}{0}Por favor aceda ao endereço {1} para efetuar a reposição da sua palavra-passe.{0}{0}Caso não tenha efetuado este pedido, por favor ignore esta mensagem.", Environment.NewLine, addr);

                            var to = new List<string>() { email };

                            Tools.Email.SendEmail(to, "Credenciais de acesso", body, WebConfigSettings.AppSettings.Email.CC, WebConfigSettings.AppSettings.Email.BCC);
                        }
                    }
                    else if (accExists.Success)
                    {
                        res.Success = false;
                    }
                    else
                    {
                        throw new Exception("Não foi possível obter um token");
                    }
                }
            }
            catch (Exception ex)
            {
                res.Error = ErrorReturnObject.Create(Guid.NewGuid(), ex.Message);
                res.Success = false;

                ErrorHandling.LogError(ex, res);
            }

            return res;
        }
        public bool ChallengeToken(string email, string token)
        {
            //[SP_User_ResetPassword_ChallengeToken]

            var res = false;

            try
            {
                if (!string.IsNullOrWhiteSpace(email))
                {
                    var accExists = AccountExists(email);

                    if (accExists.Success && (accExists as AccountExistsReturnValue).Exists)
                    {
                        using (var con = new SqlConnection(WebConfigSettings.ConnectionStrings.AcademSeguranca))
                        using (var cmd = new SqlCommand())
                        {
                            cmd.CommandText = "[SP_User_ResetPassword_ChallengeToken]";
                            cmd.Connection = con;
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            cmd.Parameters.Add("@UU", System.Data.SqlDbType.NVarChar).Value = email;
                            cmd.Parameters.Add("@tkn", System.Data.SqlDbType.NVarChar).Value = token;

                            if (con.State != System.Data.ConnectionState.Open) con.Open();

                            var reader = cmd.ExecuteReader();

                            if (reader.Read() && reader.GetColumnID("Result") == 0)
                            {
                                res = reader.GetBoolean("Result").Value;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Não foi possível obter um token");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Ocorreu um erro ao verificar o token {0} do utilizador {1}", token, email), ex);
            }

            return res;
        }
#if DEBUG
        public BasicReturnValue DeleteAccount(string email)
        {
            var res = new BasicReturnValue();

            try
            {
                if (string.IsNullOrWhiteSpace(email))
                {
                    res.Error = ErrorReturnObject.Create(Guid.NewGuid(), "O email não pode ser vazio");
                    res.Success = false;
                }
                else
                {
                    using (var con = new SqlConnection(WebConfigSettings.ConnectionStrings.AcademSeguranca))
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "SP_User_Delete_DEV";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@UU", System.Data.SqlDbType.NVarChar).Value = email;

                        if (con.State != System.Data.ConnectionState.Open) con.Open();

                        var reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            if (reader.HasColumn("Type") && reader.HasColumn("Message"))
                            {
                                throw new Exception("A eliminação de contas está desabilitada");
                            }
                        }

                        res.Success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                res.Error = ErrorReturnObject.Create(Guid.NewGuid(), ex.Message);
                res.Success = false;

                ErrorHandling.LogError(ex, res);
            }

            return res;
        }
#endif
    }
}
